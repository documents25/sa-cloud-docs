![Getting Started](./jenkins.png)

# <span style="text-transform: uppercase;"> Customizing Jenkins with plugins<span>

After unlocking Jenkins, the Customize Jenkins page appears. Here you can install any number of
useful plugins as part of your initial setup.
Click one of the two options shown:

- If you are not sure what plugins you need, choose Install suggested plugins. You
can install (or remove) additional Jenkins plugins at a later point in time via the
**Manage Jenkins > Manage Plugins** page in Jenkins.

- Install suggested plugins - to install the recommended set of plugins, which are based on most
common use cases.

- Select plugins to install - to choose which set of plugins to initially install. When you first
access the plugin selection page, the suggested plugins are selected by default.

<br>

# <span style="text-transform: uppercase;">Jenkins Pipeline <span>

## Code Example 

```bash
// Declarative //
pipeline {
     agent any ①
     stages {
        stage('Build') { ②
            steps { ③
                sh 'make' ④
            }
        }
        stage('Test'){
            steps {
                sh 'make check'
                junit 'reports/**/*.xml' ⑤
            }
        }
        stage('Deploy') {
            steps {
                sh 'make publish'
            }
        }
    }
}
// Script //
node {
     stage('Build') {
        sh 'make'
     }
    stage('Test') {
        sh 'make check'
        junit 'reports/**/*.xml'
    }
    stage('Deploy') {
        sh 'make publish'
    }
}
```
① agent indicates that Jenkins should allocate an executor and workspace for this part of the
Pipeline.

② stage describes a stage of this Pipeline.

③ steps describes the steps to be run in this stage

④ sh executes the given shell command

⑤ junit is a Pipeline step provided by the plugin:junit[JUnit plugin] for aggregating test reports

<br>

# <span style="text-transform: uppercase;"> Handling Failures <span>

Declarative Pipeline supports robust failure handling by default via its post section which allows
declaring a number of different "post conditions" such as: **always, unstable, success, failure, and
changed.** The Pipeline Syntax section provides more detail on how to use the various post conditions.

## Code Example

``` bash
pipeline {
    agent any
    stages {
         stage('Test') {
             steps {
                 sh 'make check'
             }
         }
    }
    post {
        always {
            echo 'I have finished'
            deleteDir() // clean up workspace
        }
        success {
            echo 'I succeeded!'
        }
        unstable {
            echo 'I am unstable :/'
        }
        failure {
            echo 'I failed :('
        }
        changed {
            echo 'Things are different...'
        }
    }
}
```
 post - Defines actions to be taken when pipeline or stage
completes based on outcome. Conditions execute in order:

 > - **always** - Run regardless of Pipeline status
 > - **changed**  - Run if the result of Pipeline has changed
 from last run
 > - **success**  - Run if Pipeline is successful
 > - **unstable**  - Run if Pipeline result is unstable
 > - **failure**  - Run if the Pipeline has failed

# <span style="text-transform: uppercase;"> Parameterized Pipeline<span>

Declarative Pipeline supports parameters out-of-the-box, allowing the Pipeline to accept userspecified parameters at runtime via the parameters directive. Configuring parameters with
Scripted Pipeline is done with the properties step, which can be found in the Snippet Generator.

If you configured your pipeline to accept parameters using the Build with Parameters option,
those parameters are accessible as members of the params variable.

Any Jenkins job or pipeline can be parameterized. All we need to do is check the box on the General settings tab, **“This project is parameterized”:**

> ![Getting Started](https://www.baeldung.com/wp-content/uploads/2020/10/jenkins-parameterized-builds-enable-checkbox.jpg)

## Code Example

``` bash
// Declarative //
pipeline {
    agent any
    parameters {
        string(name: 'Greeting', defaultValue: 'Hello', description: 'How should I
         greet the world?')
     }
  stages {
        stage('Example') {
             steps {
                 echo "${params.Greeting} World!"
             }
        }
    }
}
// Script //
properties([parameters([string(defaultValue: 'Hello', description: 'How should I greet
the world?', name: 'Greeting')])])

node {
     echo "${params.Greeting} World!"
}
```
Jenkins supports several parameter types. Below is a list of the most common ones, but keep in mind that different plugins may add new parameter types:

> - **String** : any combination of characters and numbers
> - **Choice**: a pre-defined set of strings from which a user can pick a value
> - **Credentials**: a pre-defined Jenkins credential
> - **File**: the full path to a file on the filesystem
> - **Multi-line String**: same as String, but allows newline characters
> - **Password**: similar to the Credentials type, but allows us to pass a plain text parameter specific to the job or pipeline
> - **Run**: an absolute URL to a single run of another job

<br>

# <span style="text-transform: uppercase;">Multibranch Pipeline <span>
```
    1. What is a Multi-branch Pipeline?
    2. Troubleshooting Multibranch Pipelines
    3. Multibranch Pipeline Best Practices
    4. Jenkins Pipeline Vs. Multibranch Pipeline
    5. Multibranch Pipeline Vs. Github Organization Job
```
## What is a Multi-branch Pipeline?

> ![Getting Started](https://devopscube.com/wp-content/uploads/2020/06/jenkins-multibranch-pipeline.jpg.webp)

A multi-branch pipeline is a concept of automatically creating Jenkins pipelines based on Git branches. It can automatically discover new branches in the source control (Github) and automatically create a pipeline for that branch. When the pipeline build starts, Jenkins uses the Jenkinsfile in that branch for build stages. 

SCM (Source Control) can be Github, Bitbucket, or a Gitlab repo.

Multi-branch pipeline supports PR based branch discovery.

For example, if you want the feature branch to run only unit testing and sonar analysis, you can have a condition to skip the deployment stage with a when a condition.

> ![Getting Started](https://devopscube.com/wp-content/uploads/2020/06/stages-condition-multi-min.png.webp)

## How Does a Multi-Branch Pipeline work?

>- Development starts with a feature branch by developers committing code to the feature branch. 
>- Whenever a developer raises a PR from the feature branch to develop a branch, a Jenkins pipeline should trigger to run a unit test and static code analysis. 
>- After testing the code successfully in the feature branch, the developer merges the PR to the develop branch.
>- When the code is ready for release, developers raise a PR from the develop branch to the master. It should trigger a build pipeline that will run the unit test cases, code analysis, push artifact, and deploys it to dev/QA environments.

>![Getting Started](https://devopscube.com/wp-content/uploads/2020/06/multibranch_pipeline_workflow-2-min.png.webp)

## Multibranch Pipleline Jenkinsfile
For the multibranch pipeline to work, you need to ave the Jenkinsfile in the SCM repo.

Lets take example ;

Note: Replace the agent label _master_ with your enkins agent name. 
```
pipeline {
  agent {
      node {
          label 'master'
      }
  }
  options {
      buildDiscarder logRotator( 
                  daysToKeepStr: '16', 
                  numToKeepStr: '10'
          )
  }
  stages {
      
      stage('Cleanup Workspace') {
          steps {
              cleanWs()
              sh """
              echo "Cleaned Up Workspace For Project"
              """
          }
      }
      stage('Code Checkout') {
          steps {
              checkout([
                  $class: 'GitSCM', 
                  branches: [[name: '*/main']], 
                  userRemoteConfigs: [[url: 'https://github.com/spring-projects/spring-petclinic.git']]
              ])
          }
      }
      stage(' Unit Testing') {
          steps {
              sh """
              echo "Running Unit Tests"
              """
          }
      }
      stage('Code Analysis') {
          steps {
              sh """
              echo "Running Code Analysis"
              """
          }
      }
      stage('Build Deploy Code') {
          when {
              branch 'develop'
          }
          steps {
              sh """
              echo "Building Artifact"
              """
              sh """
              echo "Deploying Code"
              """
          }
      }
  }   
}
```
## Setup Jenkins Multi-branch Pipeline
  
This setup will be based on Github and latest Jenkins 2.x version.

You can also use Bitbucket or Gitlab as SCM source for a multi-branch pipeline


### Create Multibranch Pipeline on Jenkins

  > - From the Jenkins home page create a “new item”. 
  > ![Getting Started](https://devopscube.com/wp-content/uploads/2020/06/jenkins-new-item.png.webp)

  > - Select the “Multibranch pipeline” from the option and click ok.
  > ![Getting Started](https://devopscube.com/wp-content/uploads/2020/06/create-multibranch-pipeline-min.png.webp)

  > - Click “Add a Source” and select Github.
  > ![Getting Started](https://devopscube.com/wp-content/uploads/2020/06/jenkins-select-Github-min.png)

  > - Under the credentials field, select Jenkins, and create a credential with your Github username and password.
  > ![Getting Started](https://devopscube.com/wp-content/uploads/2020/06/credentials-min.png)
  
  > - Select the created credentials and provide your Github repo to validate the credentials as shown below.
  > ![Getting Started](https://devopscube.com/wp-content/uploads/2020/06/validate-credentials-min.png)

  > -  Under “Behaviours” select the required option matches your requirement. You can either choose to discover all the branches in the repo or only branches with a Pull Request.
  > - The pipeline can discover branches with a PR from a forked repo as well.
  > - Choosing these options will depends on your required workflow.
  > ![Getting Started](https://devopscube.com/wp-content/uploads/2020/06/discover-branches-min.png)

> There are additional behavior you can choose from the “add” button. 
> For example, If you choose not to discover all the branches from the repo, you can opt for the regular expression or wildcard method to discover branches from the repo as shown below.

> ![Getting Started](https://devopscube.com/wp-content/uploads/2020/06/filter-options-min.png)
Here is a regex and wildcard example.
> ![Getting Started](https://devopscube.com/wp-content/uploads/2020/06/filter-examples.png)

> - If you choose to have a different name for Jenkinsfile, you can specify it in the build configuration. In the “Script Path” option, you can provide the required name. Ensure the Jenkinsfile is present in the repo with the same name you provide in the pipeline configuration.Step 7: If you choose to have a different name for Jenkinsfile, you can specify it in the build configuration. In the “Script Path” option, you can provide the required name. Ensure the Jenkinsfile is present in the repo with the same name you provide in the pipeline configuration.
> ![Getting Started](https://devopscube.com/wp-content/uploads/2020/06/multibranch-pipeline-discard-old-builds-min.png)

> - Save all the job configurations. Jenkins scans the configured Github repo for all the branches which has a PR raised.
> ![Getting Started](https://devopscube.com/wp-content/uploads/2020/06/multi-branch-pipeline-scan-min.png)


### Configure Webhook For Multibranch Pipeline : 

> - **Step 1:** Head over to the Github repo and click on the settings.
> ![Getting Started](https://devopscube.com/wp-content/uploads/2020/06/github-settings-for-webhook-min.png)

> - **Step 2:** Select the webhook option at the left and click “Add Webhook” button.
> ![Getting Started](https://devopscube.com/wp-content/uploads/2020/06/Github-Add-Webhook-min.png)

> - **Step 3:** Add your Jenkins URL followed by “/github-webhook/” under payload URL. Select the content type as “application/json” and click “Add Webhook”
> ![Getting Started](https://devopscube.com/wp-content/uploads/2020/07/create-github-webhook-for-multibranch-pipeline-min.png)
> You should see a green tick mark on a successful webhook configuration as shown below.
>![Getting Started](https://devopscube.com/wp-content/uploads/2020/06/Jenkins-Github-Successful-webhook-delivery-min.png)

>If you don’t see a green tick or see a warning sign, click on the webhook link, scroll down to “Recent Deliveries,” and click on the last webhook. You should be able to view why the webhook delivery failed with the status code.
> ![Getting Started](https://devopscube.com/wp-content/uploads/2020/06/troubleshoot-Github-webhook-delivery-min.png)

### Test Multi-branch Pipeline
>  ![Getting Started](https://devopscube.com/wp-content/uploads/2020/06/Jenkins-Github-PR-checks-min.png) 

> If you click the “Details” it will take you to the Jenkins build log. You can write custom check in your Jenkinsfile that can be used for the build reviews.

>Now, if you check Jenkins you will find a pipeline for feature branch in Jenkins as shown below.

> ![Getting Started](https://devopscube.com/wp-content/uploads/2020/06/multibranch-pipeline-branch-pipeline-trigger-min.png) 

> If the build fails, you can commit the changes to the feature branch and as long as the PR is open, it will trigger the feature pipeline.

> Here condition is being added to skip the deploy stage if the branch is not develop. You can check that in the Jenkins build log. Also, If you check the build flow in the blue ocean dashboard you can clearly see the skipped deployment stage.
> ![Getting Started](https://devopscube.com/wp-content/uploads/2020/06/Jenkinsfile-skip-stage-min.png) 

> Now merge the feature branch PR and raise a new PR from develop to the master branch.
> ![Getting Started](https://devopscube.com/wp-content/uploads/2020/06/multibranch-develop-branch-min.png) 

> For develop branch, the deploy stage is enabled and if you check the Blue Ocean build flow you can see all the stages successfully triggered.
> ![Getting Started](https://devopscube.com/wp-content/uploads/2020/06/jenkins-build-workflow-min.png) 

<br>

# <span style="text-transform: uppercase;"> PARALLEL STAGES<span>
To maximize efficiency of your Pipeline some stages can be run
in parallel if they do not depend on each other. Tests are a good
example of stages that can run in parallel.

> ![Getting Started](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTbeVc4lAZPOxj4yJ2qwGVjHJD7Q9BSBt7PGg&usqp=CAU)

## Code Example

``` bash
pipeline {
    agent any
    stages {
        stage(‘Browser Tests’) {
            parallel {
                stage(‘Chrome’) {
                    steps {
                         echo “Chrome Tests”
                    }
                }
                stage(‘Firefox’) {
                    steps {
                        echo “Firefox Tests”
                    }
                }
            }
        }
    }
}
```

<br>

# <span style="text-transform: uppercase;"> Always Backup The "JENKINS_HOME” Directory<span>
Jenkins home directory contains lots of data, including job configurations, build logs, plugin configs, etc. that we cannot afford to lose. This can be accomplished through plugins offered by Jenkins or configuring a job to take backup. Needles to say, this is one of the most essential Jenkins best practices.

## How to do?

- Backup Plugin
  > - This requires you to manually start it to back up your data.
  > - Jenkins has stopped any enhancements over it, and there are other better alternatives available over this.

- Thin Backup Plugin
  > - Install the plugin through Manage Jenkins -> Manage Plugins ->Click ‘Available’ tab -> Search for ‘Thin Backup’.
  > - Once installed, go to Manage Jenkins -> ThinBackup -> Settings.
  > - Configuration options for the plugin are self-explanatory and for any ambiguity ‘?’ beside every setting is a boon!
  > - Click ‘Backup now’ to test the backup

- Periodic Backup Plugin
  > - File Manager: Defines what files should be included in the backup and the files’ restored policy. E.g. : ConfigOnly – will choose only the configuration XML files.
  > - Storage: Specifies the method of archiving and unarchiving backups. E.g., “ZipStorage” will compress backup files into a zip archive.
  > - Location: Specifies the localization of the backups. E.g., “LocalDirectory” – will store the backup files inside the specified path.
  
  
<br>
  
# <span style="text-transform: uppercase;">Use Shared Library<span>
A shared library in Jenkins is a collection of **Groovy scripts** shared between different Jenkins jobs. To run the scripts, they are pulled into a **Jenkinsfile**.

## How to do?
- ### Create a Groovy Script
  > - First, create a Groovy script using the code you want to save to the library.
  > - ``` bash
  >   #!/usr/bin/env groovy
  >   def call(String name = 'human') {
  >   echo "Hello, ${name}."
  >   }
  >   ```

- ### Add the Script to a Git Repository
  > - Create a new Git repository containing a directory named _vars_. Save the script in the _vars_ directory as _sayHello.groovy_.
  >  ![image](https://phoenixnap.com/kb/wp-content/uploads/2022/02/jenkins-shared-library-07-git-repository.png)
  
- ### Add a Shared Library in Jenkins
  >  - Open the Jenkins dashboard in your web browser
  >  - Click the **Manage Jenkins** link on the left-hand side of the Jenkins dashboard.
  >  ![image](https://phoenixnap.com/kb/wp-content/uploads/2022/02/jenkins-shared-library-01-manage-jenkins.png)
  >  - Under _System Configuration_, click the **Configure System** button.
  >  ![image](https://phoenixnap.com/kb/wp-content/uploads/2022/02/jenkins-shared-library-02-configure-system.png)
  >   - Scroll down to the _Global Pipeline Libraries_ section and click the **Add** button.
  >  ![image](https://phoenixnap.com/kb/wp-content/uploads/2022/02/jenkins-shared-library-03-global-pipeline-libraries-add.png)
   >   - A new dialog opens prompting you to set up library details, such as name and SCM method. Fill in the details to configure the library.
   >   >   -   **Name:**  The name of the new Shared Library.
   >   >   -  **Default version:**  Set a default version for the new Shared Library. Depending on the SCM, this can be a branch name, tag, commit hash, etc.
   >   >   -  **Load implicitly:**  Checking this option allows scripts to automatically access the library without needing to request it.
   >   >   -  **Allow default version to be overridden:**  Checking this option allows scripts to select a custom version of the library.
   >   >   -  **Include @Library changes in job recent changes:**  Checking this option causes changes to the library to affect all the builds that use the library.
   >   >   -  **Cache fetched versions on master for quick retrieval:**  Checking this option makes Jenkins cache fetched versions of this library on the master.
   >   >   -  **Retrieval method:**  Choose Modern SCM (when using Jenkins SCM plugins) or Legacy SCM (when using SCM methods not integrated with Jenkins) from the drop-down menu. Selecting Modern SCM lets you choose an SCM plugin and configure relevant options.
   >   >   -  **Library Path (optional):**  Lets you set a relative path from the SCM root to the root of the library directory.
   >   - Once you are done configuring the new library, click the **Save** button to save the changes to Jenkins.
   >     ![image](https://phoenixnap.com/kb/wp-content/uploads/2022/02/jenkins-shared-library-05-save-changes.png)

- ### Use a Shared Library in a Jenkins Pipeline
  > - Create a new pipeline item in Jenkins. Type the item's name and select **Pipeline** as the project type. Click OK once done.
  >  - Add a simple script that loads the shared library. We used _Alex_ as the **`name`** parameter:
  > - ``` bash
  >   @Library('pipeline-library-demo')_
  >   stage('Demo') {
  >          echo 'Hello world'
  >          sayHello 'Alex' 
  >   }
  >   ```
  >  ![image](https://phoenixnap.com/kb/wp-content/uploads/2022/02/jenkins-shared-library-08-pipeline-script-refer-to-library.png)
  >  Click **Save** to save the changes to the pipeline.

  <br>

# <span style="text-transform: uppercase;">Wrap your input in a timeout<span>
Pipeline has an easy mechanism for timing out any given step of your pipeline. As a best practice, you should always plan for timeouts around your inputs.
- Why? For healthy cleanup of the pipeline, that's why. Wrapping your inputs in a timeout will allow them to be cleaned-up (i.e., aborted) if approvals don't occur within a given window.

## Example
> ``` bash
>   stage('Ready to Deploy') {
>       options {
>           timeout(time: 1, unit: 'MINUTES') 
>       }
>       steps {
>           input(message: "Deploy to production?")
>       }
>   }
>   ```

<br>


# Different Types of Jenkins Jobs

## Freestyle
> - Freestyle build jobs are general-purpose build jobs, which provides maximum flexibility. It can be used for any type of project.
## Pipeline
> - This project runs the entire software development workflow as code. Instead of creating several jobs for each stage of software development, you can now run the entire workflow as one code.
## Multiconfiguration
> -  The multiconfiguration project allows you to run the same build job on different environments. It is used for testing an application in different environments.

## Folder
> -  This project allows users to create folders to organize and categorize similar jobs in one folder or subfolder.

## GitHub Organization
> - This project scans your entire GitHub organization and creates Pipeline jobs for each repository containing a Jenkinsfile.

## Multibranch Pipeline
> -  This project type lets you implement different Jenkinsfiles for different branches of the same project.
  
# What are Environment Variables in Jenkins?

> -  Jenkins Environment Variable is a global variable exposed through the env variable and used anywhere in the Jenkinsfile. Any value stored in the env variable gets stored as a String type. Environment Variables can be set either at the pipeline top level, at the specific stage level, or inside the script block.
  
## _How to Set Environment Variable in a Jenkins Pipeline_
> -  Users can set Jenkins environment  variables on a global or local level. Add global environment variables through the Jenkins dashboard, while local variables are added using declarative, imperative, and scripted pipelines.
> - Jenkins Global Environment Variables
> - In Jenkins any pipeline or job can access and read global environment variables. To add a new global environment variable using the Jenkins dashboard:

> **1.** On the left-hand side of the Jenkins dashboard, click Manage Jenkins.  
>![Getting Started](https://phoenixnap.com/kb/wp-content/uploads/2021/12/jenkins-environment-variables-09-manage-jenkins.png)

> **2.** Under the System Configuration section, click Configure System.
>![Getting Started](https://phoenixnap.com/kb/wp-content/uploads/2021/12/jenkins-environment-variables-10-configure-system.png)

> **3.** Scroll down until you reach the Global properties section. Check the box next to Environment variables and click the Add button to add a new variable.

> **4.**  Enter the name and value of the new variable in the appropriate fields.
>![Getting Started](https://phoenixnap.com/kb/wp-content/uploads/2021/12/jenkins-environment-variables-11-add-environment-variable.png)

> **5.** Click the Save button to save the new variables.
    
## Reading environment variable
You can access environment variables in pipeline steps through the env object, e.g., env.BUILD_NUMBER will return the current build number. You can also use a shorthand version BUILD_NUMBER, but in this variant may be confusing to some users - it misses the context that the BUILD_NUMBER comes from the environment variable.

> ## EXAMPLE
> 
>   ```bash
>   pipeline {
>     agent any
> 
>     stages {
>         stage("Env Variables") {
>             steps {
>                 echo "The build number is ${env.BUILD_NUMBER}"
>                 echo "You can also use \${BUILD_NUMBER} -> ${BUILD_NUMBER}"
>                 sh 'echo "I can access $BUILD_NUMBER in shell command as well."'
>             }
>         }
>     }
>   }
>    ```
>   ![Getting Started](https://e.printstacktrace.blog/images/jenkins-read-env-var.png)
> 
## Setting environment variable

The environment variables can be set declaratively using environment { } block, imperatively using env.VARIABLE_NAME, or using withEnv(["VARIABLE_NAME=value"]) {} block.
     ```
> ## EXAMPLE
> 
>   ```bash
>   pipeline {
>     agent any
> 
>     environment {
>         FOO = "bar"
>     }
> 
>     stages {
>         stage("Env Variables") {
>             environment {
>                 NAME = "Alan"
>             }
> 
>             steps {
>                 echo "FOO = ${env.FOO}"
>                 echo "NAME = ${env.NAME}"
> 
>                 script {
>                     env.TEST_VARIABLE = "some test value"
>                 }
> 
>                 echo "TEST_VARIABLE = ${env.TEST_VARIABLE}"
> 
>                 withEnv(["ANOTHER_ENV_VAR=here is some value"]) {
>                     echo "ANOTHER_ENV_VAR = ${env.ANOTHER_ENV_VAR}"
>                 }
>             }
>         }
>     }
>   }
>   ```
>    ![Getting Started](https://e.printstacktrace.blog/images/jenkins-set-env-var.png)

## Overriding environment variable

Jenkins Pipeline supports overriding environment variables. There are a few rules you need to be aware of.
- The withEnv(["env=value]) { } block   can override any environment variable.
- The variables set using environment {} block cannot be overridden using imperative env.VAR = "value" assignment.
- The imperative env.VAR = "value" assignment can override only environment variables created using imperative assignment.

> ## EXAMPLE
> 
> ```bash
>     pipeline {
>     agent any
> 
>     environment {
>         FOO = "bar"
>         NAME = "Joe"
>     }
> 
>       stages {
>         stage("Env Variables") {
>             environment {
>                   NAME = "Alan" // overrides pipeline level NAME env variable
>                   BUILD_NUMBER = "2" // overrides the default BUILD_NUMBER
>             }
> 
>             steps {
>                 echo "FOO = ${env.FOO}" // prints "FOO = bar"
>                 echo "NAME = ${env.NAME}" // prints "NAME = Alan"
>                 echo "BUILD_NUMBER =  ${env.BUILD_NUMBER}" // prints "BUILD_NUMBER = 2"
> 
>                 script {
>                     env.SOMETHING = "1" // creates env.SOMETHING variable
>                 }
>             }
>         }
> 
>        stage("Override Variables") {
>             steps {
>                 script {
>                       env.FOO = "IT DOES NOT   WORK!" // it can't override env.FOO 
>                       declared at the pipeline (or stage) level
> 
>                       env.SOMETHING = "2" // it can override env variable created
>                       imperatively
>                     }
> 
>                     echo "FOO = ${env.FOO}" // prints "FOO = bar"
>                     echo "SOMETHING = ${env.SOMETHING}" // prints "SOMETHING = 2"
> 
>                     withEnv(["FOO=foobar"]) { // it can override any env variable
>                     echo "FOO = ${env.FOO}" // prints "FOO = foobar"
>                 }
> 
>                 withEnv(["BUILD_NUMBER=1"]) {
>                     echo "BUILD_NUMBER = ${env.BUILD_NUMBER}" // prints 
>                     "BUILD_NUMBER = 1"
>                 }
>              }
>          }
>       }
>   }
>   ```
> ![Getting Started](https://e.printstacktrace.blog/images/jenkins-override-env-var.png)

## Jenkins Local Environment Variable
- You can set a local environment variable in Jenkins using the declarative pipeline. This method uses the environment {} block syntax:-

   ```bash
   environment {
    [variable name] = "[variable value]"
  }
  ```
- Placing this block at the beginning of the pipeline means the variable is available for use at any step of the pipeline. Placing it at a particular stage means it is only available during the steps of that stage and that stage only.
- Another method is to use an env object in a script to imperatively define an environment variable:-

  ```bash
  script {
  env. [variable name] = "[variable value]"
  }
  ```
- Finally, using a withEnv([]) {} block sets a local environment variable as part of a scripted pipeline:-
  ```bash
   withEnv(["[variable name]= [variable value]"])
  ```
> ## EXAMPLE
>   ```bash
>     pipeline {
>       agent any
>           environment {
>             DATE = "December 17th"
>           }
>           stages {
>             stage("Env Variables") {
>               environment {
>                   NAME = "Alex"
>                }
>             steps {
>                echo "Today is ${env.DATE}"
>                echo "My name ${env.NAME}"
>                   script {
>                     env.WEBSITE = "phoenixNAP KB"
>                   } 
>                echo "This is an example for ${env.WEBSITE}"
> 
>                withEnv(["TEST_VARIABLE=TEST_VALUE"]) {
>                   echo "The value of TEST_VARIABLE is ${env.TEST_VARIABLE}"
>                 }
>             }
>           }
>         }
>       }
>   ```

- In this example, we are setting the DATE and NAME environment variables declaratively. DATE is at the top of the pipeline and can be used in every stage, while NAME is in the "Env Variables" stage, so we can only use it within that stage. The WEBSITE variable is set imperatively, and TEST_VARIABLE is a part of a scripted pipeline.
- The console output for this pipeline shows that Jenkins is able to successfully access and read every variable:-

    ![Getting Started](https://phoenixnap.com/kb/wp-content/uploads/2021/12/jenkins-environment-variables-12-local-variables-output.png)
    
## Injecting Environment Variable

Adding the EnvInject plugin to Jenkins allows you to inject environment variables during the build startup. This is particularly useful when creating a freestyle project in Jenkins.

> 1. Click Manage Jenkins on the left-hand side of the dashboard.  
> ![Getting Started](https://phoenixnap.com/kb/wp-content/uploads/2021/12/jenkins-environment-variables-13-manage-jenkins.png)

> 2.  In the System Configuration section, click the Manage Plugins button.
>![Getting Started](https://phoenixnap.com/kb/wp-content/uploads/2021/12/jenkins-environment-variables-14-manage-plugins.png)

> 3. Under the Available tab, search for envinject. Mark the checkbox next to the Environment Injector plugin and click Install without restart. 
>![Getting Started](https://phoenixnap.com/kb/wp-content/uploads/2021/12/jenkins-environment-variables-15-add-envinject-plugin.png)

> 4. Once the plugin finishes installing, return to the dashboard. Click the New Item link to create a new project, add a name, and select the Freestyle project type.
>![Getting Started](https://phoenixnap.com/kb/wp-content/uploads/2021/12/jenkins-environment-variables-16-new-freestyle-project.png)

> 5. Click OK to create a new project.

> 6. Scroll down to the Build section and click Add Build Steps to open a drop-down menu with available options. Select Inject environment variables.
>![Getting Started](https://phoenixnap.com/kb/wp-content/uploads/2021/12/jenkins-environment-variables-17-inject-environment-variables.png)

> 7. If you want to add environment variables from a properties file, add the path to the file in the Properties File Path field. Another option is to add the new variables directly to the Properties Content field, using the [variable name] = [variable value] syntax.
>![Getting Started](https://phoenixnap.com/kb/wp-content/uploads/2021/12/jenkins-environment-variables-18-add-variable-properties-content.png)

>8. Click the Save button to confirm adding the new environment variable.

<br>

# <span style="text-transform: uppercase;"> Cleaning up old Jenkins builds<span>

As a Jenkins administrator, removing old or unwanted builds keeps the Jenkins controller running efficiently. When you do not remove older builds, there are less resources for more current and relevant builds. This video reviews using the buildDiscarder directive in individual Pipeline jobs. The video also reviews the process to keep specific historical builds.
### How to delete old builds
> **Step:1** Open Jenkins project and click on configure to open configuration screen for the project.

> **Step:2** Locate the discard old builds checkbox.
> ![Getting Started](https://infoheap.com/wp-content/uploads/2016/03/jenkins-project-setting-discard-old-builds-checkbox.png)

> **Step:3** Select discard old builds checkbox to see more options. Type number of days to 10 or any other desired value. Alternatively you can also choose maximum numbers of builds to keep. Click save once done.
> ![Getting Started](https://infoheap.com/wp-content/uploads/2016/03/jenkins-discard-old-builds-settings.png)


# <span style="text-transform: uppercase;">Avoiding large global variable declaration files<span>

Having large variable declaration files can require large amounts of memory for little to no benefit, because the file is loaded for every Pipeline whether the variables are needed or not. Creating small variable files that contain only variables relevant to the current execution is recommended.

![Getting Started](https://d1whtlypfis84e.cloudfront.net/guides/wp-content/uploads/2021/03/25115617/Sample-53-1024x724.png)

## Example

```bash
#include <iostream>
using namespace std;
// Global variable declaration
int c = 12;
void test();
int main()
{
++c;
// Outputs 13
cout << c <<endl;
test();
return 0;
}
void test()
{
++c;
// Outputs 14
cout << c;
}
```

