# Jenkins pipeline email notification

  - Why do we need email notification in Jenkins?
  - How To Send Email Notification In Jenkins?

## Why do we need email notification in Jenkins? 

![Getting Started](https://d1jnx9ba8s6j9r.cloudfront.net/blog/wp-content/uploads/2020/01/222-2.png)


 
## How To Send Email Notification In Jenkins?
<br>
 <span style="color:white; font-weight: bold;
 ">Using Email Extension Plugin – </span>This plugin lets you configure every aspect of email notifications. You can customize things such as when to send the email, who receives it, and what the email says.

<span style="color:white; font-weight: bold;
 ">Using Default Email Notifier –</span> This comes with Jenkins by default. It has a default message consisting of a build number and status.

<br>

## Email Extension Plugin

> <span style="color:white; font-weight: bold;
> ">Step 1:</span> Log in to the Jenkins Homepage
> 
> <span style="color:white; font-weight: bold;
> "> Step 2:</span> Install Email Extension Plugin
>  ![Getting Started](https://d1jnx9ba8s6j9r.cloudfront.net/blog/wp-content/uploads/2020/01/1.png)
>
>  <span style="color:white; font-weight: bold;
> ">Step 3:</span> Configure System
>![Getting Started](https://d1jnx9ba8s6j9r.cloudfront.net/blog/wp-content/uploads/2020/01/2.png)
>
><span style="color:white; font-weight: bold;
> ">Step 4:</span> Create Jenkins Pipeline Job
>![Getting Started](https://d1jnx9ba8s6j9r.cloudfront.net/blog/wp-content/uploads/2020/01/3.png)
>
>Now in the pipeline section type the following code
>```
>pipeline {
>    agent any
>     
>    stages {
>        stage('Ok') {
>            steps {
>                echo "Ok"
>            }
>        }
>    }
>    post {
>        always {
>            emailext body: 'A Test EMail', recipientProviders: [[$class: 'DevelopersRecipientProvider'], [$class: 'RequesterRecipientProvider']], subject: 'Test'
>        }
>    }
>}
>```
>This pipeline runs in any Jenkins agent. It has a stage to sample. In the post step, you can >run any script you want. We have the mail sender in it. Save it and run clicking in “Build >Now” on job menu. The build will appear in the stage view.
>
>![Getting Started](https://d1jnx9ba8s6j9r.cloudfront.net/blog/wp-content/uploads/2020/01/4.png)
>
><span style="color:white; font-weight: bold;
> ">Step 7:</span> View Console Output 
>Click on Build Number “#1” and click on “Console Output” on the build menu. The output will be >like this:
>
>![Getting Started](https://d1jnx9ba8s6j9r.cloudfront.net/blog/wp-content/uploads/2020/01/5-1.png)
>
