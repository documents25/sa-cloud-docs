![Getting Started](./jenkins.png)

# Requirement
```
Minimum hardware requirements:
  • 256 MB of RAM
Sofware requirements:
  • Java 8 - either a Java Runtime Environment (JRE)
    or a Java Development Kit (JDK) is fine  
    Note: This is not a requirement if running 
          Jenkins as a Docker container.
```

## Jenkins Installation Steps

### <span style="color:gold;text-transform: uppercase;"> Step-1 Installing Jenkins</span>

The version of Jenkins included with the default Ubuntu packages is often behind the latest available version from the project itself. To ensure you have the latest fixes and features, use the project-maintained packages to install Jenkins.

> - First, add the repository key to your system:
> 
> ```
> $ wget -q -O - https://pkg.jenkins.io/debian-stable/jenkins.io.key |sudo gpg --dearmor -o /usr/share/keyrings/jenkins.gpg
> ```

> - Next, let’s append the Debian package repository address to the server’s sources.list:
> 
> ```
> $ sudo sh -c 'echo deb [signed-by=/usr/share/keyrings/jenkins.gpg] http://pkg.jenkins.io/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list'  
> ```

> - After both commands have been entered, run apt update so that apt will use the new repository.
> 
> ```
> $ sudo apt update  
> ```

> - Finally, install Jenkins and its dependencies:
> 
> ```
> $ sudo apt install jenkins
> ```


### <span style="color:gold;text-transform: uppercase;">Step-2 Starting Jenkins</span>
> - now that Jenkins is installed, start it by using systemctl:
> 
> ```
> $ sudo systemctl start jenkins.service
> ```

> - Since systemctl doesn’t display status output, we’ll use the status command to verify that Jenkins started successfully:
> 
> ```
> $ sudo systemctl status jenkins  
> ```

### <span style="color:gold;text-transform: uppercase;">Step-3 Setting Up Jenkins</span>

To set up your installation, visit Jenkins on its default port, 8080, using your server domain name or IP address: http://your_server_ip_or_domain:8080

You should receive the Unlock Jenkins screen, which displays the location of the initial password:

> ![Getting Started](https://assets.digitalocean.com/articles/jenkins-install-ubuntu-1604/unlock-jenkins.png)

> - In the terminal window, use the cat command to display the password:
> 
> ```
> $ sudo cat /var/lib/jenkins/secrets/initialAdminPassword
> ```
Copy the 32-character alphanumeric password from the terminal and paste it into the Administrator password field, then click Continue.

- The next screen presents the option of installing suggested plugins or selecting specific plugins:

> ![Getting Started](https://assets.digitalocean.com/articles/jenkins-install-ubuntu-1804/customize_jenkins_screen_two.png)

- We’ll click the Install suggested plugins option, which will immediately begin the installation process.

> ![Getting Started](https://assets.digitalocean.com/articles/jenkins-install-ubuntu-1804/jenkins_plugin_install_two.png)

- When the installation is complete, you’ll be prompted to set up the first administrative user. It’s possible to skip this step and continue as admin using the initial password from above, but we’ll take a moment to create the user.

> ![Getting Started](https://assets.digitalocean.com/articles/jenkins-install-ubuntu-1804/jenkins_create_user.png)

- You’ll receive an Instance Configuration page that will ask you to confirm the preferred URL for your Jenkins instance. Confirm either the domain name for your server or your server’s IP address:

> ![Getting Started](https://assets.digitalocean.com/articles/jenkins-install-ubuntu-1804/instance_confirmation.png)

- Click Start using Jenkins to visit the main Jenkins dashboard:

> ![Getting Started](https://assets.digitalocean.com/articles/jenkins-install-ubuntu-2204/jenkins_home_page.png)
