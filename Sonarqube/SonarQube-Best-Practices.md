# SonarQube Best Practices


SonarQube is an automatic code review tool to detect bugs, vulnerabilities, and code smells in your code. It can integrate with your existing workflow to enable continuous code inspection across your project branches and pull requests.

When a piece of code does not comply with a rule, an issue is logged on the snapshot. An issue can be logged on a source file or a unit test file.

There are 3 types of issues: Bugs, Code Smells and Vulnerabilities.

Issue  |  Description
------------- | -------------
Bug  | A reliability-related issue that represents something wrong in the code. If this has not broken yet, it will, and probably at the worst possible moment. This needs to be fixed. Yesterday.
Code Smell | A maintainability-related issue in the code. Leaving it as-is means that at best maintainers will have a harder time than they should making changes to the code. At worst, they'll be so confused by the state of the code that they'll introduce additional errors as they make changes.
Vulnerability | A security-related issue which represents a backdoor for attackers See also [Security-related rules](https://docs.sonarqube.org/latest/user-guide/security-rules/ "Named link title")
Security Hotspot | Security-sensitive pieces of code that need to be manually reviewed. Upon review, you'll either find that there is no threat or that there is vulnerable code that needs to be fixed.

## Coverage  
* (coverage) It is a mix of Line coverage and Condition coverage. Its goal is to provide an even more accurate answer to the following question: How much of the source code has been covered by the unit tests?

## Condition coverage 
(branch_coverage) On each line of code containing some boolean expressions, the condition coverage simply answers the following question: 'Has each boolean expression been evaluated both to true and false?'. This is the density of possible conditions in flow control structures that have been followed during unit tests execution.

## Line coverage
* (linecoverage) On a given line of code, Line coverage simply answers the following question: Has this line of code been executed during the execution of the unit tests? It is the density of covered lines by unit tests: Line coverage = LC / EL where LC = covered lines (linestocover - uncoveredlines) EL = total number of executable lines (linestocover)

* More details on coverage can be found here:
https://docs.sonarqube.org/latest/analysis/coverage/


## Quality Profiles 
* Quality Gates are the set of conditions a project must meet before it should be pushed to further environments. Quality Gates considers all of the quality metrics for a project and assigns a passed or failed designation for that project. It is possible to set a default Quality Gate which will be applied to all projects not explicitly assigned to some other gate.

* Sets of rules to check against the source code for things such as: programming errors, bugs, stylistic errors, complexity concerns, and suspicious constructs. These are the ones included by default

* Sonar way o default for each language, intended to be expanded upon

* Sonar way recommended o only for Typescript and Javascript. Has ~50% more rules. Extremely limited security benefit as most are formatting/verb based and do not fall under the OWASP Top 10, SANS Top 25 or CWE controls areas.

* Extended rules o you can create custom rules, but the base sets give pretty good coverage for what is meant as an assessment for common static vulnerabilities

### Custom Quality Profile Setup
* Click on the Quality Gates Tab

![pic-01](images/SonarQube-QualityGate-1.png)

* Some default quality gates are already there. You can use any one of them for your project. Administrators can also create new Quality Gates.

* Clicking any of the Quality Gates will show the criteria used for that quality gate

![pic-02](images/SonarQube-QualityGate-2.png)

#### Changing the Quality Gate for a Project
* You can choose which quality gate to use for your project if you do not want to use the default gate.
* Open your project in SonarQube.
* Go to the Administration > Quality Gate menu for project

![pic-03](images/SonarQube-QualityGate-3.png)

* Choose the quality gate you want to use for that project

![pic-04](images/SonarQube-QualityGate-4.png)


## Pipeline integration

Details on pipeline integration can be found at https://docs.sonarqube.org/latest/analysis/branch-pr-analysis-overview/ NOTE: Merge/Pull request analysis requires the Developer, Enterprise, or Data Centre Edition (paid)


## Local clone scan
If you do scan against a local clone of the repository on your workstation or laptop, you may need to install additional packages for the scans to perform successfully (e.g. ```**npm install typescript**``` into the base scan directory). If you don’t, SonarQube will not be able to perform analysis for that language.


## Quality Gates

Based on the results obtained through the scan, the scan will either Pass or Fail. Quality Gates help determine the desired threshold for your code. These are set in both local and cloud-based builds. There is a default quality gate, shown below, which should be applicable for clean as you code development cycles. https://docs.sonarqube.org/latest/user-guide/quality-gates/


## Conditions on New Code
(Sonar way default quality gate)

Metric|Operator|Value
----------|--------|--------
Coverage	|is less than	| 80.0%
Duplicated Lines (%) |	is greater than |	3.0%
Maintainability Rating	is | worse than	| A
Reliability Rating	is | worse than |	A
Security Rating	is | worse than |	A

While these quality gates will not stop a build from going forward, or being promoted to a subsequent environment, any issue that triggers a value beyond the default should be examined. This is especially critical for Bugs and Security issues.

If security issues are identified in your scan and you are unsure of how to proceed, please contact your [Ministry information security team](https://logon7.gov.bc.ca/clp-cgi/int/logon.cgi?flags=1000:1,0&TARGET=$SM$https%3a%2f%2fintranet%2egov%2ebc%2eca%2fthehub%2focio%2focio-enterprise-services%2finformation-security-branch%2fmiso-contacts%3f) or inquire in the #infosec channel on Rocket.Chat.


## Things to consider…

* If you don’t setup for day 0, technical debt will build up and seem unmanageable - Language analyzers are not installed by default! - Make sure to install the language analyzers for code in your repositories o Administration > Marketplace o Don’t install/run them all if you don’t need to. Only having the languages enabled you need to assess reduces check time and chance of error when running the scans - Use secret scanning integration with the Source Code Management (SCM) system to prevent secrets from getting pushed to the repo before SonarQube even has a chance to see them. This is available on GitHub by default for public repos (https://docs.github.com/en/code-security/secret-security/about-secret-scanning).

* Infrastructure as Code (IoC) analysis is NOT covered by SonarQube (e.g. Ansible, Terraform)

* For IoC analysis, please investigate solutions such as: - https://geekflare.com/iac-security-scanner/ - Checkov, TFLint, Terrafirma, Accurics, CloudSploit - https://snyk.io/product/infrastructure-as-code-security/


