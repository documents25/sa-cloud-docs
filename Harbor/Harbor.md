# Harbor 

![Getting Started](https://miro.medium.com/max/871/1*FEChFwTaaiClBo8qBypm1g.jpeg)

# What is Harbor?  

![Getting Started](https://miro.medium.com/max/1400/1*y265BPxQlbnyNwVxkaNTpQ.png)  


- Harbor is an open source registry that secures artifacts with policies and role-based access control, ensures images are scanned and free from vulnerabilities, and signs images as trusted. Harbor, a CNCF Graduated project, delivers compliance, performance, and interoperability to help you consistently and securely manage artifacts across cloud native compute platforms like Kubernetes and Docker.

- As illustrated in the Diagram, Consider deploying an External Instance of the Harbor Registry, with all its cool features. This Instance of the harbor registry could act as a Point of Truthfor the Images and OCI artifacts in the CI/CD infrastructure. The External Harbor can run as a Kubernetes Service on the TKG clusters (preferred way) or on a Virtual Machine.

- There are multiple types of distributions — TKG Extensions, Bitnami Helm Charts, Harbor Compose, etc. — that are available to deploy Harbor Registry. You may refer to some of the URLs provided at the end of the article, for the installation steps.


# Prerequisites
- Domain mapped to A record to get letsencrypt certificate. ( If you have your own certificate no need this)
- Internet connection to the server.
- Latest version of docker and docker-compose installed.

# Steps :-
# <span style="color:white ;font-weight: bold;text-transform: uppercase;"> Configuration </span>

- First of all update your package repository
```
 $ sudo apt update
```

- Harbor can be accessed in a secure environment using TLS encryption. For this, we are going to generate a letsencrypt certificate. You need to install the certbot package to get the certificate.

  ( If you are going to use your own certificate then exclude the step )
```
$ sudo apt install certbot -y
```

- Now generate a certificate, use your domain name and email.

  ( If you are going to use your own certificate then exclude the step )
```
$ sudo certbot certonly --standalone -d "harbor.yourdomain.com" --preferred-challenges http --agree-tos -n -m "admin@yourdomain.com" --keep-until-expiring
```

- By default certificate are generated in /etc/letsencrypt/live/harbor.yourdoain.com/

- Let’s begin to download the harbor package. Copy the following script and paste it in your terminal. It will download the latest version of harbor.
```
$ curl -s https://api.github.com/repos/goharbor/harbor/releases/latest | grep browser_download_url | cut -d '"' -f 4 | grep '\.tgz$' | wget -i -
```

- Extract the content using the following command.
``` 
$ tar zxvf harbor-offline-installer-v*.tgz
```

- You will get a harbor directory, change to it.
```
$ cd harbor
```

- You can see the installation script file and other configurations. An example of configuration is given in a temp file. Just copy it to the harbor.yml file. 
```
$ cp harbor.yml.tmpl harbor.yml
``` 

- Open the harbor.yml file and make necessary changes like harbor your hostname, http and https port, certificate path and admin dashboard password. If you have your own certificate then make sure they are in the path you defined.
```
$ vim harbor.yml
```
> ![Getting Started](https://linuxways.net/wp-content/uploads/2021/06/word-image-465.png)


- Also in the same file you can configure database password, maximum number of connections and volume to store your images. Save the file finally.

> ![Getting Started](https://linuxways.net/wp-content/uploads/2021/06/word-image-466.png)


- Now, run the install script as shown below. The notary and chartmuseum are optional.

- Notary helps to digitally sign images using keys that verify content and publish them securely. Chartmuseum provides helm chart repositories.
```
$ sudo ./install.sh --with-notary --with-chartmuseum
```

- After the installation finished. Confirm by listing the port below.
```
$ sudo ss -tulpan | grep -i list
```
> ![Getting Started](https://linuxways.net/wp-content/uploads/2021/06/word-image-467.png)


- Also verify that there is some content in your data volume. And see log directory.
```
$ ls /data

$ ls /var/log/harbor/
```
> ![Getting Started](https://linuxways.net/wp-content/uploads/2021/06/word-image-468.png)

# <span style="color:white ;font-weight: bold;text-transform: uppercase;"> Harbor web access</span>


- Now you can access the harbor web. Type https://harbor.yourdomain.com

- To access harbor web use the credential you defined in above harbor.yml configuration.

> ![Getting Started](https://linuxways.net/wp-content/uploads/2021/06/word-image-469.png)

# <span style="color:white ;font-weight: bold;text-transform: uppercase;">Harbor dashboard</span>


- The dashboard is really nice and easy to use. You can create project and send your image to store and use it in the deployment.

> ![Getting Started](https://linuxways.net/wp-content/uploads/2021/06/word-image-470.png)

# <span style="color:white ;font-weight: bold;text-transform: uppercase;">Create project and username in Harbor
</span>


- Harbor has two types of project:

     1. Public: all users can access this repository, it is used 
                for string public images.
  
     2. Private: only authorized users can access the repository.

- Click new project to create new one

> ![Getting Started](https://www.gosysops.com/wp-content/uploads/2020/04/image-20200417212936881.png)


- Enter project name, access level, and quota.

> ![Getting Started](https://www.gosysops.com/wp-content/uploads/2020/04/image-20200417213037912.png)


- Create users


> ![Getting Started](https://www.gosysops.com/wp-content/uploads/2020/04/image-20200417213119286.png)


- Input the basic information.

> ![Getting Started](https://www.gosysops.com/wp-content/uploads/2020/04/image-20200417213225125.png)


- Add a user to be a member of group by clicking Projects->Members->User

> ![Getting Started](https://www.gosysops.com/wp-content/uploads/2020/04/image-20200417213324966.png)


- Assign role permission

> ![Getting Started](https://www.gosysops.com/wp-content/uploads/2020/04/image-20200417213410090.png)

 
