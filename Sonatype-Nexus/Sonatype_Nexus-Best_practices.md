# **Infrastructure-based Best Practices**

## **Resiliency and High Availability**
    Migrate to the external PostgreSQL Database should be recommended to every customer who is not yet already.  The biggest benefit is to create hot backups.

## **System Requirements**
    	

Few requirements to keep in mind:

- CPU / RAM / Disk Space
    
- Java version
    
- Instance Memory Sizing Profiles
    
- File Systems
    - Embedded data
    - Blob Stores
    
- Web Browser
    
- File Handle Limits
    
- Run as a service
   
- JVM Heap size - Configuration Examples based on RAM

## **Backup and Restore**
    
- Set up internal documentation on backup and failover action plans.  Assign owners of the tasks and review them regularly for changes.
    
- Plan for testing failures at least once a year; quarterly is highly recommended.

## **Upgrade**

- Download and Compatibility

- Release Notes

- Ensure to get familiar with the Nexus Repository Manager 
    Directories

- Ensure to create a valid backup

- Set up internal documentation on upgrade, backup, and failover action plans; assign owners of the tasks and review them regularly for any changes.

## **Service User (Run as a Service)**

    Ensure when Nexus Repository Manager is installed, the application runs as a service.  It is best practice to create a service user that runs the application.

## **Runtime Environment**

    Configure the runtime environment to the deployed instance to ensure to maintain the stability, performance, and overall health of the application.

# **Nexus Repository Manager-based Best Practices**

## **Anonymous Access**

Follow some of the best practices:

- The Anonymous Role should be disabled when restricting user access to any repository is required.

- The Default Anonymous Role has access to all repositories; a new role should be created with the least privileges to allow which repositories are allowed.

- If specific content needs to view access restricted, preferable to create repositories for specific teams.

## **Blob Store Configuration**

Blob Store configuration takes careful planning.  These decisions should be based on the following:

- The size of your repositories

- The rate at which you expect them to grow over time

- The storage space available

- The options you have available for adding storage space

Additional information:

- Blob Store API
- Setting soft quotas:
    
    - Space Limit - A limit (i.e., Constraint Limit (in MB)) compared against specific blob store metrics by a soft quota
    
    - Space Used  Quota - This type of quota is violated when the total size of the blob store exceeds the space limit
    
    - Space Remaining Quota - This type of quota is violated when the available space of a blob store falls below the space limit
    
- Tasks:

    - Admin - Cleanup unused asset blobs

    - Admin - Compact blob store

    - Repair - Reconcile component database from blob store

## **Cleanup**

This is so important because:

- Disk space can grow out of hand
- More space will mean added costs
- It will be harder for devs to find what is important
- Vulnerable components remain in the repo longer than they should
- Backup time will increase

An internal cleanup strategy should be developed based on repository usage, available storage space, and component size.  It is further recommended that cleanup and related policies are determined before disk space issues arise and the situation becomes reactive.

    => Note:

    Anything deleted by cleanup policies is soft deleted.  Disk space is not reclaimed until an Admin - Compact blob store task is run.  The frequency and/or automation of that task should be done to your level of caution.  See here for more general information on tasks.

    Despite the soft deletion, misconfiguration of a cleanup policy can result in the removal of most or even all of your components so caution (and use of preview) is recommended.

## **Monitoring**

Monitoring is a key part of keeping the application in good health.  Some monitoring that should be in place are:

- Tracking what components are coming in

- Where are the components being stored (which repositories)

- At what rate are the components being added verses removed from the 
repository

- Blob stores - Adding a Soft Quota: configure to raise alerts when it exceeds a configured constraint

- Log files - process to handle as part of general maintenance

## **Routing Rules**

    Create routing rules on all proxy repositories as a best practice.  This will speed up read access on group repositories and better protect the build environment from being exposed to unwanted components from public proxies.

    => NOTE:  
    Administrators can use routing rules to limit requests to external repositories to only the artifacts that are needed from that proxy. Routing rules can help with dependency confusion like attacks against an organization’s internal namespace. Routing rules will speed up access times for group repositories by limiting requests to only the proxies where the components should be fetched.

## **Scripting**

If scripts are being used, ask the following questions to the customer:

- Why do you still need these scripts?

- When were these scripts added?

- How often are these scripts used?

- What services do these scripts call?

- Where are these scripts sourced?

- Who owns and maintains these scripts?

- Are these scripts reviewed regularly if any internal workings are modified?

```
What Can I Use Instead of Scripts?

NXRM now has extensive REST APIs that accomplish many of the same objectives previously only available by using custom scripts.  We suggest browsing the in-product REST API documentation under Administration -> System -> API for the complete list of supported endpoints.

Operations such as cleaning up repository storage is a core feature.

If you think you have a scripting use case that others might benefit from, please check our documentation for a similar feature.  An improvement request may already exist for a matching feature - vote for it and comment to express your interest.  If you can't find an existing improvement request, file one of your own for us to consider.
```

## **Staging**

The concepts behind staging come from requirements for modern CI/CD pipelines which are not managed effectively with just a single repository.  A few common requirements are:

- Create a release candidate to test or discard before being promoted
- Keep build metadata for release artifacts
- Avoid dependencies on non-production artifacts
- Clean up fast based on artifact lifecycle

Some quick benefits to staging:

- Repository endpoints at each stage in the pipeline
- Artifacts are not copied or duplicated
- Everything as a release candidate
- Promotion workflow is driven by CI
- Cleanup is automated based on stage policy

## **Tagging**

Tagging provides the ability to mark a set of components with a tag so that they can be logically associated with each other.  The most common scenarios for using tags include the following:

- A CI build ID for a project (e.g., project-abc-build-142)

- A higher-level release train when you are coordinating multiple projects together as a single unit (e.g., release-train-13)

The following are some best practice recommendations for using tagging:

- Use short tag names and attributes to maximize the performance of tag-related operations and storage utilization.  Maintaining a large number of tags with attributes of maximum allowable size (20k) could result in undesirable performance degradation.

- Proactively use the Admin - Cleanup tags task to clean up unused tags.  The Admin - Cleanup tags task provides configuration options to support routine removal of aged or unused tags and the associated components (if desired).

- Use a tag naming scheme that will ensure the uniqueness of created tags and prevent naming collisions and potential interruptions to CI pipeline workflows.
