> ![Getting Started](https://cdn.hashnode.com/res/hashnode/image/upload/v1648922226106/ReijKvWX1.png)

# **Nexus** 

## **What is an Artifact?**

```
The files that contain both the compiled code and the resources that are used to compile them are known as artifacts. They are readily deployable files.

In java an artifact would be a .jar, .war, .ear file, in NPM the artifact file would be a .tar.gz file, in .NET the artifact file would be a .dll file.
```

## **What is an Artifact Repository?**

```
An artifact repository is a repository that can store multiple versions of artifacts. Each time a .war file or .tar.gz file is created, it is stored in a server dedicated for the artifacts
```
## **What is Nexus?**

```
Nexus by Sonatype is a repository manager that organizes, stores and distributes artifacts needed for development. With Nexus, developers can completely control access to, and deployment of, every artifact in an organization from a single location, making it easier to distribute software.
```
> ![Getting Started](https://cloudogu.com/images/blog/2015/05/nexusSystem.png)

## **Nexus Repository: Responsibilities**

```
1. Helps in storing readily deployable code
2. Helps in downloading and managing dependencies
```

# **Installation and Configuration**

## **How to Install Nexus Repository on Ubuntu 20.04 LTS**

## Update the system packages
    sudo apt-get update

## 1: Install OpenJDK 1.8 on Ubuntu 20.04 LTS
    sudo apt install openjdk-8-jre-headless

## 2: Download Nexus Repository Manager setup on Ubuntu 20.04 LTS
    cd /opt
    
Download the SonaType Nexus on Ubuntu using wget
    
    sudo wget https://download.sonatype.com/nexus/3/latest-unix.tar.gz

## 3: Install Nexus Repository on Ubuntu 20.04 LTS

Extract the Nexus repository setup in /opt directory
    
    tar -zxvf latest-unix.tar.gz
 
Rename the extracted Nexus setup folder to nexus
    
    sudo mv /opt/nexus-3.30.1-01 /opt/nexus
 
As security practice, not to run nexus service using root user, so lets create new user named nexus to run nexus service

    sudo adduser nexus


To set no password for nexus user open the visudo file in ubuntu

    sudo visudo 
 
Add below line into it , save and exit

    nexus ALL=(ALL) NOPASSWD: ALL
Give permission to nexus files and nexus directory to nexus user

    sudo chown -R nexus:nexus /opt/nexus
    sudo chown -R nexus:nexus /opt/sonatype-work
 
To run nexus as service at boot time, open /opt/nexus/bin/nexus.rc file, uncomment it and add nexus user as shown below

    sudo nano /opt/nexus/bin/nexus.rc
    run_as_user="nexus"
 
To increase the nexus JVM heap size, open the /opt/nexus/bin/nexus.vmoptions file, you can modify the size as shown below

 Add the below content to that file.

    -Xms1024m
    -Xmx1024m
    -XX:MaxDirectMemorySize=1024m
    -XX:LogFile=./sonatype-work/nexus3/log/jvm.log
    -XX:-OmitStackTraceInFastThrow
    -Djava.net.preferIPv4Stack=true
    -Dkaraf.home=.
    -Dkaraf.base=.
    -Dkaraf.etc=etc/karaf
    -Djava.util.logging.config.file=/etc/karaf/java.util.logging.properties
    -Dkaraf.data=./sonatype-work/nexus3
    -Dkaraf.log=./sonatype-work/nexus3/log
    -Djava.io.tmpdir=./sonatype-work/nexus3/tmp

 
## 4: Run Nexus as a service using Systemd

To run nexus as service using Systemd

    sudo nano /etc/systemd/system/nexus.service

paste the below lines into it.

    [Unit]
    Description=nexus service
    After=network.target
    [Service]
    Type=forking
    LimitNOFILE=65536
    ExecStart=/opt/nexus/bin/nexus start
    ExecStop=/opt/nexus/bin/nexus stop
    User=nexus
    Restart=on-abort
    [Install]
    WantedBy=multi-user.target
 
To start nexus service using systemctl

    sudo systemctl start nexus

To enable nexus service at system startup

    sudo systemctl enable nexus

To check nexus service status

    sudo systemctl status nexus

To stop Nexus service
    
    sudo systemctl stop nexus

If the nexus service is not started, you can the nexus logs using below command

    tail -f /opt/sonatype-work/nexus3/log/nexus.log
 
## 5: Access Nexus Repository Web Interface

To access Nexus repository web interface , open your favorite browser
if you are running UFW firewall on Ubuntu, open the firewall port 8081 using below command

    ufw allow 8081/tcp
    http://server_IP:8081

you will see below default nexus page

To login to Nexus, click on Sign In, default username is admin

To find default password run the below command

    cat /opt/nexus/sonatype-work/nexus3/admin.password
    