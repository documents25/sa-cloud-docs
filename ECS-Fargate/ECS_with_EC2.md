![Getting Started](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRzYHdTEPfAU1Ih_5F2veQC8K9pmNTNJGk38w&usqp=CAU)

# <span style="text-transform: uppercase;"> Deploy on ecs with pipeline<span>

Amazon Elastic Container Service (ECS) is a cloud computing service in Amazon Web Services (AWS) that manages containers and allows developers to run applications in the cloud without having to configure an environment for the code to run in. It enables developers with AWS accounts to deploy and manage scalable applications that run on groups of servers -- called clusters -- through application program interface (API) calls and task definitions. 

![Getting Started](https://d1.awsstatic.com/product-page-diagram_Amazon-ECS%402x.0d872eb6fb782ddc733a27d2bb9db795fed71185.png)

<br>

# <span style="text-transform: uppercase;">Create ECR repository <span>

Before you can push your Docker or Open Container Initiative (OCI) images to Amazon ECR, you must create a repository to store them in. Public repositories are visible on the Amazon ECR Public Gallery and are open to publicly pull images from. If you want to create a private repository that also we can create.

### <span style="color:gold;text-transform: uppercase;"> Step-1 navigation to ecr page</span>
 
> - Go to the ECR homepage and click the Get Started button.
> ![Getting Started](https://earthly.dev/blog/assets/images/how-to-setup-and-use-amazons-elastic-container-registry/xX4GGwL.png)
> 
 ### <span style="color:gold;text-transform: uppercase;"> Step-2 Create repository</span>
> 
 > - You will be taken to the Create repository page, where you can enter all the details for your new repository.
> 
 >  ![Getting Started](https://earthly.dev/blog/assets/images/how-to-setup-and-use-amazons-elastic-container-registry/SZrf8AQ.png)

 Choose a visibility (Public or Private), name the repository (ideally something short but self-explanatory), and below the name, select any of the other options you need:

> -  Tag immutability - Prevents the same tag from being pushed twice and overwriting a previous version of the tag.
> 
> -  Scan on push - Your images will be scanned for security vulnerabilities each time a new tag is pushed.
> -  KMS encryption - Allows you to use AWS Key Management Service (KMS) to encrypt the images in this repository.
 
 **Note: your final repository URL structure will be something like this:**
 
> ```<account-id>.dkr.ecr.<account-region>.amazonaws.com/<repository-name>```
 
 **Once your repository is configured, click Create repository to initialize your ECR repository. If you want to create a public repository, you can follow the same steps but select Pubic instead of Private.**
> 
### <span style="color:gold;text-transform: uppercase;"> Step-3 repository created</span>

 > ![Getting Started](https://earthly.dev/blog/assets/images/how-to-setup-and-use-amazons-elastic-container-registry/XCZe4vX.png)

 ### <span style="color:gold;text-transform: uppercase;"> Step-4 push image to repository </span>

 > - Select the repository that you created and choose View push commands to view the steps to push an image to your new repository.
>
> Before publishing the Image to ECR, make sure you have Docker installed on your workstation and a project with a Dockerfile that’s ready to be built and pushed to ECR.
>
> **If you don’t have a project ready, create a new file called Dockerfile**

<br>

 ### <span style="color:gold;text-transform: uppercase;"> Dockerfile</span>

```dockerfile
FROM node:14-alpine
WORKDIR /node-app
COPY . .
RUN npm install
EXPOSE 3000
CMD [ "node", "server.js" ]
```
> ![Getting Started](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR2m3vMf1Q1BTN7Gvu0o_UVcRyxbROpp384EVoyFcRipqabr_a64UjBcXAPnNNvEWX97Dk&usqp=CAU)

<br>

# <span style="text-transform: uppercase;">Create ecs cluster <span>

### <span style="color:gold;text-transform: uppercase;">create Cluster</span>

> - First, log into the management console and go to the ECS services.
> ![Getting Started](./images/ec2-console-1.png)

<br>

> - From the left side panel on the console, click on the Clusters button.
>
> ![Getting Started](./images/cluster-left-nv.png)

<br>

> - Now click on the Create cluster to start creating the cluster.
> ![Getting Started](./images/create-cluster.png)

<br>

> Now first, it will ask for the ECS cluster template. There are three templates available for the ECS cluster.
> - Networking only
> - EC2 Linux + Networking
> - EC2 Windows + Networking
> ![Getting Started](./images/select-ec2-template-1.png)

The **Networking** only template creates clusters using AWS Fargate. AWS Fargate is the latest technology provided by AWS to deploy ECS clusters. **AWS Fargate is a serverless service to deploy ECS clusters, and you do not need to manage the nodes inside the cluster**. Nodes are managed by AWS, and you only provide task definitions for the service. For this template, you only create the ECS cluster, and VPC and subnets are optional for this.

The **EC2 Linux + Networking template** creates the ECS cluster, **including the Nodes running Linux AMI**. In order to run an ECS cluster using the EC2 Linux + Networking template, you need to create the Cluster, VPC, subnets, and Auto scaling group with Linux AMIs. The auto-scaling group is used to manage the nodes in the cluster.

The **EC2 Windows + Networking template creates the ECS cluster with Windows AMIs**. You create the Cluster, subnets, VPC, and auto-scaling group with Windows AMIs. The windows instances are managed and scaled by the auto-scaling group.

<br>

> - Give a name for the cluster, select Provisioning model and instance type. For this example, I will use 3 On-Demand t2.micro EC2 instances. Select SSH key pair if you want to have access to instances in the cluster.
> ![Getting Started](./images/configure-cluster.png)

<br>

> - In the networking section select VPC and subnets, then select security group created for the cluster then press Create.
> ![Getting Started](./images/security-vpc.png)

<br>

> -AWS ECS will spread EC2 instances in your cluster over 3 AZ. You can check a list of instances on the EC2 instances page.
>  ![Getting Started](./images/instance-list.png)

<br>

### <span style="color:gold;text-transform: uppercase;">Create Task Definition</span>

>- Now we need to create a task definition. Go to **ECS -> Clusters -> Task Definition -> Create new Task Definition**, select EC2 type and press Next step.
>
>  ![Getting Started](./images/task-defination.png)

<br>

>- Give it a name and select defaults for Task Role and Task execution role.
>
>  ![Getting Started](./images/task-role.png)

<br>

>- Define a Task memory and Task CPU. These two configurations depending on the requirements of the application running inside the container. Then press Add container.
>
>  ![Getting Started](./images/add-container.png)

<br>

>- The new window will open. Give the name for the container, for the image past URI of your image. **The image used for this tutorial is located in AWS ECR**. 
>
>- Set the memory limit. In Port, mappings fill only the Container port and omit **the Host port (or set it to 0) and your container automatically receives a port** in the ephemeral port range for your container instance. The rest of the configurations live as default.
>  ![Getting Started](./images/container-url.png)

<br>

### <span style="color:gold;text-transform: uppercase;">Create Service</span>

>- On the cluster level on the **Service tab press Create**. Select Launch type as EC2, select previously created task definition, fill-up service name, select service type as Replica, number of tasks
>
>  ![Getting Started](./images/configure-service.png)

<br>

>- Then select Deployment type as **Rolling update.**
>
>  ![Getting Started](./images/service-2.png)

<br>

>- Select Load balancer type as the Application load balancer.
>
>  ![Getting Started](./images/service-3.png)

<br>

>- Then select default service role and load balancer and target group, and press Create.
>
>  ![Getting Started](./images/service-4.png)

<br>

>- To check the tasks of this service go to Clusters -> Cluster-name ->Tasks tab.
>
>  ![Getting Started](./images/service-5.png)

<br>

>- To check the app go to EC2 -> Load Balancing -> Load Balancers Open a load balancer of a cluster on a Description Tab copy a DNS name.
>  ![Getting Started](./images/service-6.png)
>
>- Then paste it into a browser and you will see the output.

<br>

# <span style="text-transform: uppercase;">Create Gitlab pipeline <span>

In GitLab, runners are agents that run your CI/CD jobs.

Ensure you have runners available to run your jobs.If you don’t have a runner, install GitLab Runner and register a runner for your instance, project, or group.

### <span style="color:gold;text-transform: uppercase;"> Step-1 view available runners </span>

> **To view available runners:**
> 
> - **Go to Settings > CI/CD and expand Runners.**
>
> ![Getting Started](./images/runner-active.png)

As long as you have at least one runner that’s active, with a green circle next to it, you have a runner available to process your jobs.

### <span style="color:gold;text-transform: uppercase;"> Step-2 Create a .gitlab-ci.yml file</span>

In this file, you define:
> 
> - The structure and order of jobs that the runner should execute.
> - The decisions the runner should make when specific conditions are encountered.

To create a .gitlab-ci.yml file:
> - On the left sidebar, **select Project information > Details.**
> - Above the file list, select the branch you want to commit to, **select the plus icon, then select New file:**
> ![Getting Started](./images/new_file_v13_6.png)

For the Filename, type .gitlab-ci.yml and in the larger window, paste this sample code:

## gitlab-ci.yml File

```yaml
stages:         
  - build
  - deploy

production-create-image-job:  
  tags: 
    - runner1
  stage: build    
  script:
    - echo "login to the ECR"
    - aws ecr get-login-password --region $AWS_DEFAULT_REGION | docker login 
      --username AWS --password-stdin 
      $AWS_ACCOUNT_ID.dkr.ecr.$AWS_DEFAULT_REGION.amazonaws.com
    - echo "login successfully"
    - echo "building image"
    - docker build -t 
      $AWS_ACCOUNT_ID.dkr.ecr.$AWS_DEFAULT_REGION.amazonaws.com/$IMAGE_NAME:latest .
    - echo "image build successfully"
    - docker push 
      $AWS_ACCOUNT_ID.dkr.ecr.$AWS_DEFAULT_REGION.amazonaws.com/$IMAGE_NAME:latest
    - echo "image pushed successfully"
  only:
    - production

production-deploy-new-image:  
  tags: 
    - runner1
  stage: deploy    
  script:
    - aws ecs stop-task --cluster node-cluster --task $(aws ecs list-tasks --cluster
      node-cluster --service node-deploy-service --output text --query taskArns[0])
    - aws ecs stop-task --cluster node-cluster --task $(aws ecs list-tasks --cluster
      node-cluster --service node-deploy-service --output text --query taskArns[1])
    - echo "update the service"
  only:
    - production

```
- **$AWS_DEFAULT_REGION, $AWS_ACCOUNT_ID, $IMAGE_NAME are predefined variables that populate when the job runs.**

### <span style="color:gold;text-transform: uppercase;"> Step-3 Commit the code</span>

> - Select Commit changes.
> 
> - The pipeline starts when the commit is committed.

### .gitlab-ci.yml tips
> 
> After you create your first .gitlab-ci.yml file, use the pipeline editor for all future edits to the file. With the pipeline editor, you can:
> 
> - Edit the pipeline configuration with automatic syntax highlighting and validation.
> 
> - View the CI/CD configuration visualization, a graphical representation of your .gitlab-ci.yml file.

### <span style="color:gold;text-transform: uppercase;"> Step-4  View the status of your pipeline and jobs</span>


> To view your pipeline:
> 
> - Go to CI/CD > Pipelines.
 
> A pipeline with two stages should be displayed:
> 
> ![Getting Started](./images/pipeline.png)
 
> To view a visual representation of your pipeline, select the pipeline ID.
> 
> ![Getting Started](./images/stages-pipeline.png)


<br>

# <span style="text-transform: uppercase;">Grafan and prometheus monitoring<span>

### <span style="color:gold;text-transform: uppercase;"> Step-1 Modify the daemon.json file</span>

> - In order for Prometheus to gather the metrics of the docker we need to add below stanza in the **/etc/docker/daemon.json** file in the docker server.
> 
> ```
> {
>   "metrics-addr" : "127.0.0.1:9323",
>   "experimental" : true
> }
> ```
> - **9323 is the port from where Prometheus will gather the metrics**

<br>

>- After adding above stanza we need to restart the docker using below command
>```
>$ sudo systemctl restart docker
>```
>- We can view the different metrics at the  <span style="color:green;text-transform: uppercase;"><Public IP:9393/metrics> </span>

<br>

### <span style="color:gold;text-transform: uppercase;"> Step-2 Install Prometheus</span>

- First, download and unpack the current stable version of Prometheus into your home directory. You can find the latest binaries along with their checksums on the Prometheus download page.

```
$ sudo wget https://github.com/prometheus/prometheus/releases/download/v2.36.2/prometheus-2.36.2.linux-amd64.tar.gz
 ```
 - Now, unpack the downloaded archive

 ```
 $ sudo tar xf prometheus-2.36.2.linux-amd64.tar.gz 
 
 $ sudo mv prometheus-2.36.2.linux-amd64/ prometheus

 $ cd prometheus/
 ```

 - Run the following command in Terminal to edit the configuration file:

 ```
 $ sudo nano prometheus.yml
 ```
 - Copy and paste the following lines into the terminal:

 ```
 - job_name: "docker"
     static_configs:
        	    - targets: ["Public IP of Docker Node:9323"]
 ```

<br>

### <span style="color:gold;text-transform: uppercase;"> Step-3 Install Grafana</span>

> - But first, add the GPG key which will allow you to install signed packages
> 
> ```
> curl https://packages.grafana.com/gpg.key | sudo apt-key add -
> ```

<br>

>- Next, add the Grafana APT repository as shown
>
>```
>sudo add-apt-repository "deb https://packages.grafana.com/oss/deb stable main"
>```

<br>

>- Once the repository has been added, update the system repositories
>```
>sudo apt update
>```
<br>

>- Next, install Grafana server
>```
>sudo apt install grafana -y
>```
<br>

>- By default, Grafana should be running by default once installed. Additionally, you can check it’s status by running
>```
>systemctl status grafana-server
>```

<br>

>- If grafana is disabled, you can start it an enable it on boot by executing:
>```
>systemctl start grafana-server
>
>systemctl enable grafana-server
>```
<br>

>- Allowing port 3000 on the UFW firewall
>```
>ufw allow 3000/tcp
>
>ufw  reload
>
>ufw status
>```

> - Accessing Grafana Server
> 
> ```
> https://server-IP:3000/
> ```
> 
> This displays a login page as shown below
> 
> ![Getting Started](https://adamtheautomator.com/wp-content/uploads/2022/05/image-250.png)

<br>


>- The default credentials are :
>```
>username: admin
>password: admin
>```

<br>

>- Once logged in, you’ll be on the Grafana home page shown below.
>
>![Getting Started](https://adamtheautomator.com/wp-content/uploads/2022/05/image-251-1536x1101.png)
