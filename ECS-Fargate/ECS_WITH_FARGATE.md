## Created By:- 
              Viraj Patel
              Dharmik Nakrani
              Yashvi Shah
              Krima Vyas
              Ami kalola

![Getting Started](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRzYHdTEPfAU1Ih_5F2veQC8K9pmNTNJGk38w&usqp=CAU)

# <span style="text-transform: uppercase;"> Deploy on ecs with pipeline<span>

Amazon Elastic Container Service (ECS) is a cloud computing service in Amazon Web Services (AWS) that manages containers and allows developers to run applications in the cloud without having to configure an environment for the code to run in. It enables developers with AWS accounts to deploy and manage scalable applications that run on groups of servers -- called clusters -- through application program interface (API) calls and task definitions. 

![Getting Started](https://d1.awsstatic.com/product-page-diagram_Amazon-ECS%402x.0d872eb6fb782ddc733a27d2bb9db795fed71185.png)

<br>

# <span style="text-transform: uppercase;">Create ECR repository <span>

Before you can push your Docker or Open Container Initiative (OCI) images to Amazon ECR, you must create a repository to store them in. Public repositories are visible on the Amazon ECR Public Gallery and are open to publicly pull images from. If you want to create a private repository that also we can create.

### <span style="color:gold;text-transform: uppercase;"> Step-1 navigation to ecr page</span>
 
> - Go to the ECR homepage and click the Get Started button.
> ![Getting Started](https://earthly.dev/blog/assets/images/how-to-setup-and-use-amazons-elastic-container-registry/xX4GGwL.png)
> 
 ### <span style="color:gold;text-transform: uppercase;"> Step-2 Create repository</span>
> 
 > - You will be taken to the Create repository page, where you can enter all the details for your new repository.
> 
 >  ![Getting Started](https://earthly.dev/blog/assets/images/how-to-setup-and-use-amazons-elastic-container-registry/SZrf8AQ.png)

 Choose a visibility (Public or Private), name the repository (ideally something short but self-explanatory), and below the name, select any of the other options you need:

> -  Tag immutability - Prevents the same tag from being pushed twice and overwriting a previous version of the tag.
> 
> -  Scan on push - Your images will be scanned for security vulnerabilities each time a new tag is pushed.
> -  KMS encryption - Allows you to use AWS Key Management Service (KMS) to encrypt the images in this repository.
 
 **Note: your final repository URL structure will be something like this:**
 
> ```<account-id>.dkr.ecr.<account-region>.amazonaws.com/<repository-name>```
 
 **Once your repository is configured, click Create repository to initialize your ECR repository. If you want to create a public repository, you can follow the same steps but select Pubic instead of Private.**
> 
### <span style="color:gold;text-transform: uppercase;"> Step-3 repository created</span>

 > ![Getting Started](https://earthly.dev/blog/assets/images/how-to-setup-and-use-amazons-elastic-container-registry/XCZe4vX.png)

 ### <span style="color:gold;text-transform: uppercase;"> Step-4 push image to repository </span>

 > - Select the repository that you created and choose View push commands to view the steps to push an image to your new repository.
>
> Before publishing the Image to ECR, make sure you have Docker installed on your workstation and a project with a Dockerfile that’s ready to be built and pushed to ECR.
>
> **If you don’t have a project ready, create a new file called Dockerfile**

<br>

 ### <span style="color:gold;text-transform: uppercase;"> Dockerfile</span>

```dockerfile
FROM node:14-alpine
WORKDIR /node-app
COPY . .
RUN npm install
EXPOSE 3000
CMD [ "node", "server.js" ]
```
> ![Getting Started](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR2m3vMf1Q1BTN7Gvu0o_UVcRyxbROpp384EVoyFcRipqabr_a64UjBcXAPnNNvEWX97Dk&usqp=CAU)

<br>

# <span style="text-transform: uppercase;">Create ecs cluster with fargate <span>

### <span style="color:gold;text-transform: uppercase;">create Cluster</span>

> - First, log into the management console and go to the ECS services.
> ![Getting Started](./images/ec2-console-1.png)

<br>

> - From the left side panel on the console, click on the Clusters button.
>
> ![Getting Started](./images/cluster-left-nv.png)

<br>

> - Now click on the Create cluster to start creating the cluster.
> ![Getting Started](./images/create-cluster.png)

<br>

> Now first, it will ask for the ECS cluster template. There are three templates available for the ECS cluster.
> - Networking only
> - EC2 Linux + Networking
> - EC2 Windows + Networking

**AWS recommends using the AWS Fargate to run the ECS cluster, and it is the latest technology among these. So for this demo, we will use the AWS Fargate to create the ECS cluster.**

> ![Getting Started](https://linuxhint.com/wp-content/uploads/2022/01/word-image-681.png)

The **Networking** only template creates clusters using AWS Fargate. AWS Fargate is the latest technology provided by AWS to deploy ECS clusters. **AWS Fargate is a serverless service to deploy ECS clusters, and you do not need to manage the nodes inside the cluster**. Nodes are managed by AWS, and you only provide task definitions for the service. For this template, you only create the ECS cluster, and VPC and subnets are optional for this.

The **EC2 Linux + Networking template** creates the ECS cluster, **including the Nodes running Linux AMI**. In order to run an ECS cluster using the EC2 Linux + Networking template, you need to create the Cluster, VPC, subnets, and Auto scaling group with Linux AMIs. The auto-scaling group is used to manage the nodes in the cluster.

The **EC2 Windows + Networking template creates the ECS cluster with Windows AMIs**. You create the Cluster, subnets, VPC, and auto-scaling group with Windows AMIs. The windows instances are managed and scaled by the auto-scaling group.

<br>

> - After selecting the option, now click on the **Next step button** at the bottom right corner of the page.
>
> - It will ask for the different configurations for the ECS cluster. The name is the unique identifier for the ECS cluster, and the same name can not be used for another ECS cluster in the same region.
> ![Getting Started](https://linuxhint.com/wp-content/uploads/2022/01/word-image-687.png)

<br>

### <span style="color:gold;text-transform: uppercase;">Create Task Definition</span>

<br>

> - After creating the ECS cluster, now create an ECS task definition to deploy a sample container on the ECS cluster.
> ![Getting Started](https://linuxhint.com/wp-content/uploads/2022/01/word-image-694.png)

<br>

> -First, it will ask for launch type compatibility for the task definition. There are three launch-type compatibilities for the task definitions.
> 
> - Fargate
> - EC2
> - External
> 
> The Fargate launch type compatibility is used for AWS-managed infrastructure, and there is no need to deploy any EC2 instance. This launch-type compatibility is used for ECS clusters using AWS Fargate. The cost is based on the container size.
>  ![Getting Started](https://linuxhint.com/wp-content/uploads/2022/01/word-image-699.png)

<br>

>- Enter a unique name for the task definition to be created. Task role is used to make API calls to the AWS services. For this demo, leave the task role to none. Operating system family is the OS which the task definition will use. For this demo, select Linux as the operating system family.
>
>  ![Getting Started](https://linuxhint.com/wp-content/uploads/2022/01/word-image-701.png)

<br>

>- The task size is the memory and the number of vCPUs that will be allocated to the container for execution. For this demo, allocate 0.5 GB of RAM and 0.25 vCPU.
>
>  ![Getting Started](https://linuxhint.com/wp-content/uploads/2022/01/word-image-705.png)

<br>

>- After allocating RAM and vCPUs, now click on the add container button to add a container. Enter the name of the container and the image that will be used by the container.
>
> - You can also specify the hard and soft limits of the resources allocated to the container. If a hard limit is specified, the container will be killed if it exceeds that limit. If a soft limit is specified, the container will reserve that amount of memory.
> 
> - Port mapping is used to access container ports on the host machine. For this demo, set the port 80.
> 
>  ![Getting Started](https://linuxhint.com/wp-content/uploads/2022/01/word-image-707.png)

<br>

>- After creating the task definition, now go to the Task Definitions from the left side panel of the ECS console. Select the newly created task definition and run it using the Run task option from the Actions list.
>  ![Getting Started](https://linuxhint.com/wp-content/uploads/2022/01/word-image-709.png)

<br>

>- It will ask for the different options for the container before running. Provide the Fargate as launch type as we will use Fargat as launch type compatibility.
>
> - Select Linux as the operating system of the container and provide the other details, as shown in the following image.
>  ![Getting Started](https://linuxhint.com/wp-content/uploads/2022/01/word-image-711.png)

<br>

>- Select the VPC, subnet, and security groups you want to assign to the task definition.
>  ![Getting Started](https://linuxhint.com/wp-content/uploads/2022/01/word-image-713.png)

<br>

>- After providing all this detail, now click on the run button to run the task definition. After running the task definition, now check the status of the task definition from the console
>  ![Getting Started](https://linuxhint.com/wp-content/uploads/2022/01/word-image-715.png)

<br>

### <span style="color:gold;text-transform: uppercase;">Attach load balancer with fargate</span>

<br>

>- Application load balancer receives traffic and forwards the traffic to the target groups. These target groups are the groups of the targets like EC2 instances in multiple availability zones.
>
>- This section will create a target group and then register the EC2 instance to the target group. First, log into the AWS management console and go to the EC2 services.
>
>  ![Getting Started](https://linuxhint.com/wp-content/uploads/2021/12/image3-9.png)

<br>

> - From the top right corner of the console, click on the create target group button to create a new target group.
>
> ![Getting Started](https://linuxhint.com/wp-content/uploads/2021/12/image2-10.png)

<br>

>- You can create your TG when you create your ALB (ALB is called dddd in my example), or beforehand. ALso, I named my target group my-tg-for-fargate. I used port 80 (you probably need 5000) for my container.**Make sure to create IP target type, not instance. Fargete will not work with instance TGs**
>  ![Getting Started](https://i.stack.imgur.com/NH3RL.png)

<br>

> - After creating the target group for the load balancer, now go to the Load balancers from the left side panel.
>
>  ![Getting Started](https://linuxhint.com/wp-content/uploads/2021/12/image6-8.png)

<br>

> - It will open a new web page to select the load balancer type to create. Select the application load balancer and click on the create button.
>
>  ![Getting Started](https://linuxhint.com/wp-content/uploads/2021/12/image8-7.png)

<br>

> - Now it will ask for the basic configuration of the application load balancer. Enter the Load balancer name, scheme, and IP address type. Load balancer name is a unique identifier for the application load balancer to be created.
>
>  ![Getting Started](https://linuxhint.com/wp-content/uploads/2021/12/image12-5.png)

<br>

> - Now for networking, select the VPC, availability zones, and subnets from availability zones. The VPC must be the same as selected while creating the target group.
>
>  ![Getting Started](https://linuxhint.com/wp-content/uploads/2021/12/image10-7.png)

<br>

> - For the security group, select a security group from the VPC that will control inbound and outbound traffic from the application load balancer.
>
>  ![Getting Started](https://linuxhint.com/wp-content/uploads/2021/12/image11-7.png)

<br>

> - For this demo, we will configure port 80 of the application load balancer to receive traffic from the end-users and forward the traffic to the demo-tg target group.
>
>  ![Getting Started](https://linuxhint.com/wp-content/uploads/2021/12/image13-5.png)

<br>

> - After configuring the load balancer, now have a look at the configuration summary and click on the create load balancer to create the load balancer.
>
>  ![Getting Started](https://linuxhint.com/wp-content/uploads/2021/12/image14-5.png)

<br>

>- When you create your Fargate service in ECS console, you will have option to choose existing ALB (in my case dddd) and existing target group (in my case called my-tg-for-fargate. You don't need to create second tg:
>  ![Getting Started](https://i.stack.imgur.com/OrHA6.png)

<br>

# <span style="text-transform: uppercase;">Create Gitlab pipeline <span>

In GitLab, runners are agents that run your CI/CD jobs.

Ensure you have runners available to run your jobs.If you don’t have a runner, install GitLab Runner and register a runner for your instance, project, or group.

### <span style="color:gold;text-transform: uppercase;"> Step-1 view available runners </span>

> **To view available runners:**
> 
> - **Go to Settings > CI/CD and expand Runners.**
>
> ![Getting Started](./images/runner-active.png)

As long as you have at least one runner that’s active, with a green circle next to it, you have a runner available to process your jobs.

### <span style="color:gold;text-transform: uppercase;"> Step-2 Create a .gitlab-ci.yml file</span>

In this file, you define:
> 
> - The structure and order of jobs that the runner should execute.
> - The decisions the runner should make when specific conditions are encountered.

To create a .gitlab-ci.yml file:
> - On the left sidebar, **select Project information > Details.**
> - Above the file list, select the branch you want to commit to, **select the plus icon, then select New file:**
> ![Getting Started](./images/new_file_v13_6.png)

For the Filename, type .gitlab-ci.yml and in the larger window, paste this sample code:

## gitlab-ci.yml File

```yaml
stages:
  - ECR
  - ECS

before_script:
  - aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin ${ECR_PRIVATE_REPO}

image-build-job:  
  tags: 
    - fargate
  stage: ECR    
  script:
    - docker build -t ${ECR_PRIVATE_REPO}/node-service-two:latest .
    - echo "Build Images"
  only:
    - stage

image-deploy-job: 
  tags: 
    - fargate

  stage: ECR 
  script:
    - docker push ${ECR_PRIVATE_REPO}/node-service-two:latest
    - docker images
    - echo "Deploy Images"
  only:
    - stage

image-remove-job: 
  tags: 
    - fargate
  stage: ECR 
  script:
    - docker rmi -f ${ECR_PRIVATE_REPO}/node-service-two:latest
    - docker images
    - echo "Remove Images"
  only:
    - stage


ecs-update-job: 
  tags: 
    - fargate
  stage: ECS 
  script:
    - aws ecs update-service --cluster fargate-1 --service fargate-service --force-new-deployment --task-definition fargate-td
    - echo "Update Serive"
  only:
    - stage



```
- **$ECR_PRIVATE_REPO is predefined variables that populate when the job runs.**

### <span style="color:gold;text-transform: uppercase;"> Step-3 Commit the code</span>

> - Select Commit changes.
> 
> - The pipeline starts when the commit is committed.

### .gitlab-ci.yml tips
> 
> After you create your first .gitlab-ci.yml file, use the pipeline editor for all future edits to the file. With the pipeline editor, you can:
> 
> - Edit the pipeline configuration with automatic syntax highlighting and validation.
> 
> - View the CI/CD configuration visualization, a graphical representation of your .gitlab-ci.yml file.

### <span style="color:gold;text-transform: uppercase;"> Step-4  View the status of your pipeline and jobs</span>


> To view your pipeline:
> 
> - Go to CI/CD > Pipelines.
 
> A pipeline with two stages should be displayed:
> 
> ![Getting Started](./images/fargate_pipeline.png)
 
<br>