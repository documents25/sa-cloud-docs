
![Getting Started](https://miro.medium.com/max/1400/1*KcMUgKVn8ztNspLJOkwPHQ.png)

# ZAP

ZAP (ZED Attack Proxy) — is an open-source proxy tools like Burp which is used in Security Assessments of web apps. It offers various features like Scanner, Fuzzer , REST Api and lot more. A new interesting feature is ZAP Heads Up Display (HUD) which is really interesting.

## why do we use zap
```
 1. AJAX spidering
 2. ZAP Jenkins Plugin
 3. Websocket Testing
 4. Highly Scriptable
 5. Flexible Scan Policy Management
 6. Interacting With ZAP Programmatically via the REST API
```

# Jenkins

Jenkins is an open source automation server widely used for CI / CD purposes.

In this post I will walkthrough installation of ZAP and Jenkins in a Ubuntu / Linux Operating system so that I can explain about issues and its solutions occur during the process.

# How to Integrate ZAP with Jenkins

![Getting Started](https://uploads-ssl.webflow.com/6225a414ab1e86e4cd4c71d0/6279053e02b1485f08b1f885_JenkinsZAP_final_image.png)


  ```
 OWASP’s Zed Attack Proxy (ZAP) is one of the most widely used application security scanners. The following manual describes the short steps involved in integrating the OWASP ZAP plugin with Jenkinsthe world's favourite CI / CD platform.
  ```

## <span style="color:gold ;font-weight: bold;text-transform: uppercase;"> ZAP integrate with jenkins uisng jenkinsfile  </span>

## Prerequisites
- You will need to have Jenkins installed.
- Docker should be installed in the Jenkins machine

## Steps 

# <span style="color:white ;font-weight: bold;text-transform: uppercase;">Pipeline in Jenkins</span>

 <span style="color:white ;font-weight: bold;text-transform: uppercase;"> STEP 1: Create a new pipeline </span>

 ```
Let’s tick the option “This project is parameterized”.
 ```
![Getting Started](https://miro.medium.com/max/1138/1*xm1Ml9QpsV4XydBbi69PNA.png)

<span style="color:white ;font-weight: bold;text-transform: uppercase;"> STEP 2: We add the script mentioned at the beginning (copy & paste the script) </span>


![Getting Started](https://miro.medium.com/max/1138/1*V910dGmI4tsa-R1H7q8zFA.png)

 ```
 def scan_type
 def target
 pipeline {
     agent any
     parameters {
         choice  choices: ["Baseline", "APIS", "Full"],
                 description: 'Type of scan that is going to perform inside the container',
                 name: 'SCAN_TYPE'
 
         string defaultValue: "https://example.com",
                 description: 'Target URL to scan',
                 name: 'TARGET'
 
         booleanParam defaultValue: true,
                 description: 'Parameter to know if wanna generate report.',
                 name: 'GENERATE_REPORT'
     }
     stages {
         stage('Pipeline Info') {
                 steps {
                     script {
                         echo "<--Parameter Initialization-->"
                         echo """
                         The current parameters are:
                             Scan Type: ${params.SCAN_TYPE}
                             Target: ${params.TARGET}
                             Generate report: ${params.GENERATE_REPORT}
                         """
                     }
                 }
         }
 
         stage('Setting up OWASP ZAP docker container') {
             steps {
                 script {
                         echo "Pulling up last OWASP ZAP container --> Start"
                         sh 'docker pull owasp/zap2docker-stable'
                         echo "Pulling up last VMS container --> End"
                         echo "Starting container --> Start"
                         sh """
                         docker run -dt --name owasp \
                         owasp/zap2docker-stable \
                         /bin/bash
                         """
                 }
             }
         }
 
 
         stage('Prepare wrk directory') {
             when {
                         environment name : 'GENERATE_REPORT', value: 'true'
             }
             steps {
                 script {
                         sh """
                             docker exec owasp \
                             mkdir /zap/wrk
                         """
                     }
                 }
         }
 
 
         stage('Scanning target on owasp container') {
             steps {
                 script {
                     scan_type = "${params.SCAN_TYPE}"
                     echo "----> scan_type: $scan_type"
                     target = "${params.TARGET}"
                     if(scan_type == "Baseline"){
                         sh """
                             docker exec owasp \
                             zap-baseline.py \
                             -t $target \
                             -x report.xml \
                             -I
                         """
                     }
                     else if(scan_type == "APIS"){
                         sh """
                             docker exec owasp \
                             zap-api-scan.py \
                             -t $target \
                             -x report.xml \
                             -I
                         """
                     }
                     else if(scan_type == "Full"){
                         sh """
                             docker exec owasp \
                             zap-full-scan.py \
                             -t $target \
                             //-x report.xml
                             -I
                         """
                         //-x report-$(date +%d-%b-%Y).xml
                     }
                     else{
                         echo "Something went wrong..."
                     }
                 }
             }
         }
         stage('Copy Report to Workspace'){
             steps {
                 script {
                     sh '''
                         docker cp owasp:/zap/wrk/report.xml ${WORKSPACE}/report.xml
                     '''
                 }
             }
         }
     }
     post {
             always {
                 echo "Removing container"
                 sh '''
                     docker stop owasp
                     docker rm owasp
                 '''
             }
         }
 }
 ```
<span style="color:white ;font-weight: bold;text-transform: uppercase;"> STEP 3: We save and execute by selecting “build now”. Once the “build with Parameters” option appears, we proceed to cancel it.</span>

![Getting Started](https://miro.medium.com/max/442/1*fMA6ezD6hL4xKxKrvthMnA.png)


<span style="color:white ;font-weight: bold;text-transform: uppercase;"> STEP 4:  Select “Build with parameters”, add the scan options and execute the build until it is finished.</span>

![Getting Started](https://miro.medium.com/max/624/1*wNJ6vx6zW3fCKmkSiFASAg.png)


<span style="color:white ;font-weight: bold;text-transform: uppercase;"> STEP 5:If we previously chose the option to generate a report, it will be found in the Workspace folder when the pipeline is executed.
</span>

![Getting Started](https://miro.medium.com/max/1138/1*4CpRqCcRJ0oZeu4Qp28QUQ.png)

# Output report

![Getting Started](./images/zap-output.png) 



<br>

## <span style="color:gold ;font-weight: bold;text-transform: uppercase;"> Integrate ZAP with Jenkins Using plugins  </span>
##  

## Prerequisites
- Java (JDK & JRE)

## Steps 
 <span style="color:white ;font-weight: bold;text-transform: uppercase;"> STEP 1: ZAP Jenkins Plugin </span>
```
To integrate ZAP with Jenkins, you’ll first need the ZAP Jenkins plugin. You can get that under Manage Jenkins -> Manage Plugins. Install OWASP ZAP Official plugin under Available Tab.
```

![Getting Started](https://uploads-ssl.webflow.com/6225a414ab1e86e4cd4c71d0/62750a329f62bcb67cf8045c_1.png) 

<span style="color:white ;font-weight: bold;text-transform: uppercase;"> STEP 2: Installing ZAP Locally
</span>
```
In addition to the plugin, you’ll also need to install ZAP in your local machine. If you already have ZAP, you can skip this step.

Under Manage Jenkins -> Global Tool Configuration, click on Custom Tool installation. Under Custom tool Section; provide OWASP ZAP tar downloadable link and the directory name. (The actual download will happen in step 7, so don’t worry about it right now)

Note: If you do not have the Custom Tool Installation option, you need to download a plugin called “Custom Tool Installation”. To get that, follow the same process as in Step 1
```

![Getting Started](https://uploads-ssl.webflow.com/6225a414ab1e86e4cd4c71d0/62750a32343133386a45d96a_2.png) 

 
<span style="color:white ;font-weight: bold;text-transform: uppercase;"> STEP 3: Running ZAP on Jenkins </span>
```
By now, you should have ZAP and its plugin. Moving forward, you’ll need to configure two essential things; namely ZAP host and port. Go to Manage Jenkins -> Configure System and fill the ZAP HOST and Port field under ZAP section.
```

![Getting Started](https://uploads-ssl.webflow.com/6225a414ab1e86e4cd4c71d0/62750a329f62bc2a69f8045d_3.png) 

<span style="color:white ;font-weight: bold;text-transform: uppercase;"> STEP 4: Create New Project</span>
```
    Click New Item and create a new Job as Freestyle Project. Click OK

```
![Getting Started](https://uploads-ssl.webflow.com/6225a414ab1e86e4cd4c71d0/62750a32b2e64be8ba5a3856_4.png) 

<span style="color:white ;font-weight: bold;text-transform: uppercase;"> STEP 5: Save Project</span>
```
   Click Save without making any configuration changes for the Job.
```
![Getting Started](https://uploads-ssl.webflow.com/6225a414ab1e86e4cd4c71d0/62750a32b2e64be8ba5a3856_4.png) 

<span style="color:white ;font-weight: bold;text-transform: uppercase;"> STEP 6: Create Workspace</span>
```
  Now, click Build Now to create a workspace on the master machine.
```
![Getting Started](https://uploads-ssl.webflow.com/6225a414ab1e86e4cd4c71d0/62750a320a586a21cf226230_6.png) 

<span style="color:white ;font-weight: bold;text-transform: uppercase;"> STEP 7: Configure ZAP</span>
```
  Go to ZAP Project Configure Page. In my case, it would be under ZAP_CI_Demo project. Click Configure | Select Build Environment. Check the Install Custom Tools option, and select the ZAP tool.
```
![Getting Started](https://uploads-ssl.webflow.com/6225a414ab1e86e4cd4c71d0/62750a33c0efab7d1007ffc2_7.png) 


<span style="color:white ;font-weight: bold;text-transform: uppercase;"> STEP 8: Execute ZAP</span>
```
Select Build Tab under project configurations | Click Add build step | Select Execute ZAP
```
![Getting Started](https://uploads-ssl.webflow.com/6225a414ab1e86e4cd4c71d0/62750a332ccafd0b69eecd77_8.png) 

<span style="color:white ;font-weight: bold;text-transform: uppercase;">STEP 9.1: Installing ZAP </span>
```
If you had ZAP before this blog, move to step 9.2

Use the Installation Method option to specify how ZAP will be installed on the master machine and specify the absolute location of the ZAP Home Directory where would you like to create, or you can specify .ZAP as relative path for ZAP Home Directory. It will automatically create the .ZAP folder on the Jenkins Custom Tools Directory (/var/lib/jenkins/tools/../.ZAP/)

Note: If you specified custom path for ZAP Home Directory, make sure Jenkins has the needed permission to create a directory on the specified path. For Example: If the Jenkins user does not have permission to create directory on /home/ , you'll have to create the directory manually and change the owner of the folder to the Jenkins user.
```
![Getting Started](https://uploads-ssl.webflow.com/6225a414ab1e86e4cd4c71d0/62750a3376ecdc3ee92fbaab_9.png) 

<span style="color:white ;font-weight: bold;text-transform: uppercase;">STEP 9.2: Find ZAP Installation Directory</span>
```
 If you already had ZAP, select the “System Installed: ZAP Installation Directory” option, and let the environment variable input remain default.

Now, to specify the ZAP installed path for the environment variable (ZAPROXY_HOME).

9.2.1: Go to Manage Jenkins -> Configure System and Select Environment variable checkbox under Global Properties. Click Add

9.2.2: Enter the name as ZAPROXY_HOME and give the value as ZAP installed path. In this example, it would be the path /opt/zaproxy. Save the changes.    
```
![Getting Started](https://uploads-ssl.webflow.com/6225a414ab1e86e4cd4c71d0/62750a332bed0151fda294db_10.png) 

```
Then continue with your ZAP Configuration. Under ZAP Home Directory, provide the path along with “.ZAP”. (ZAP Home Directory is already created while running the ZAP for the first time in your local machine. In this example it would be /home/umar/.ZAP)
```
![Getting Started](https://uploads-ssl.webflow.com/6225a414ab1e86e4cd4c71d0/62750a33da2ad924d8867748_11.png) 

<span style="color:white ;font-weight: bold;text-transform: uppercase;">STEP 10: Testing</span>
```
So far, you’ve connected ZAP with Jenkins and configured it such that ZAP is triggered during the build process. Now, you need to provide information on application or URL that needs testing. This part is still done under BUILD tab.

10.1: For that, you would need to create Persist session in ZAP under Session Management section.

10.2: Next, you’ll need to provide application name and URL under Session Properties Options in regex format. Under the Include in Context, supply the URL. Under the Exclude in Context, you should provide the parameter that does not need an active scan or spidering.

For Example:

Include in Context: http://testphp.vulnweb.com

where Testphp.vulnweb.com is the Target Application and * indicates all paths across the application

Exclude from Context (optional): ^(?:(?!http://testphp.vulnweb.com).*).$
```
![Getting Started](https://uploads-ssl.webflow.com/6225a414ab1e86e4cd4c71d0/62750a33a52588e291b6e85a_12.png) 

```
In the above example, we’ve provided a URL for testing in the Include in Context input field. In the Exclude in Context field, we’ve provided a regex that will exclude everything that is not within the scope.

Alright! Let’s take a breath and see what we’ve achieved. So far, you’ve downloaded the required plugins, connected ZAP to Jenkins, set ZAP to trigger during build, and provided the testing session details. Now, you need to configure the scan policies and spidering of the application, because the goal of this integration is to scan the application with specific to certain scanning policies.
```

<span style="color:white ;font-weight: bold;text-transform: uppercase;"> STEP 11: Attack Mode</span>
```
Under the Attack Mode section of the build tab, enter the URL in Starting Point field for spidering the application, and select Spider scan option. Let the rest under this section remain default. Enable Active Scan checkbox and select the Policy from the dropdown list. If you don’t have any options in the dropdown list, which will be the case if it’s your first time running ZAP, it will consider the Default Policy for the active scan.
```
![Getting Started](https://uploads-ssl.webflow.com/6225a414ab1e86e4cd4c71d0/62750a3447f24248232dc34f_13.png) 

<span style="color:white ;font-weight: bold;text-transform: uppercase;"> STEP 12: Generate Report</span>
```

Tick Generate Report check box and provide all information to generate report in HTML and XML Format. You need to give a unique filename for every iteration of scans. Example: Consider filename as

JENKINS_ZAP_VULNERABILITY_REPORT_${BUILD_ID}

JENKINS_ZAP_VULNERABILITY_REPORT_ : This is a constant prefix

${BUILD_ID}  : This is the Jenkins Environment variable, which is always unique for on the current build.
```

![Getting Started](https://uploads-ssl.webflow.com/6225a414ab1e86e4cd4c71d0/62750a34c0efabc30b07ffd5_14.png) 

<span style="color:white ;font-weight: bold;text-transform: uppercase;"> STEP 13: Post-build Actions</span>
```
Under Post-build Actions:

13.1: Click Add Post-Build Action, and select Archive the artifacts. In the input field under the Archive the Artifacts, add the following directories, which should be separated by commas. (You can copy paste it from below as is)

   logs/*,reports/*

13.2: click Add Post-Build Action, and select Publish HTML Reports. Then, click Add, and input the directory where the HTML reports are stored. Under the Index page input field, specify the file’s name as used in step 12, along with extension .html. In this example, use

JENKINS_ZAP_VULNERABILITY_REPORT_${BUILD_ID}.html

```
![Getting Started](https://uploads-ssl.webflow.com/6225a414ab1e86e4cd4c71d0/62750a340e08bb51c1f7702c_15.png) 


<span style="color:white ;font-weight: bold;text-transform: uppercase;"> STEP 14: Complete Build</span>
```
Click on Save and Click Build Now. Once build is completed, you can view the HTML Report on Job Dashboard and other archived files from workspace.

```
![Getting Started](https://uploads-ssl.webflow.com/6225a414ab1e86e4cd4c71d0/62750a34d4a76996fa3c4de1_16.png) 

```
Finally, let’s look at an example pipeline workflow with ZAP integrated as part of the mainstream pipeline      
```
![Getting Started](https://uploads-ssl.webflow.com/6225a414ab1e86e4cd4c71d0/62750a34efef315b7b90d93f_17.jpeg) 

```
In the above image, blue arrows indicate the flow of pipeline, and numbers indicate the build process.

1.Developer makes a new commit to the code repository.

2.Assuming that the Github repository is already integrated with Jenkins CI service using a Jenkins Webhook, this will trigger the Github Checkout Build process.

3.Once the Github Checkout Build is completed, it will initiate the ZAP build process to automate the DAST scan against deployed Environment.

4.ZAP scan results will then be archived in Jenkins and pushed to Orchestron (in this case) for result correlation.

5.Application Security Engineer & Developer can automatically raise ticket in Jira using results from Orchestron.

6.With that in perspective, follow the next steps to finish up the integration.
```

<span style="color:white ;font-weight: bold;text-transform: uppercase;"> STEP 15: Build Triggers
</span>
```
Go to ZAP Project, click Configure and select Build trigger tab. Under the Build Trigger section, select one of the option which preferable for the CI/CD Pipeline.

In our case, use the ‘Build after other projects are built’ checkbox option and enter the Project to watch field. It will trigger the ZAP Job after that project build process is completed.

You need to select your conditional statement that is suitable for the project which you are watching.

- Trigger only if build is stable
- Trigger even if the build is unstable
- Trigger even if the build fails

```
![Getting Started](https://uploads-ssl.webflow.com/6225a414ab1e86e4cd4c71d0/62750a3417e4ee4a6cf3b5a7_18.png) 

```
Make sure to save the changes.

Now, you have successfully integrated ZAP with your Jenkins pipeline. The ZAP Build process will run continuously along with your existing CI Pipeline whenever a new commit made on the Github repository.

You're all set!

```
