# ZAP Document

![Getting Started](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQbkKfXBF8bKA0xLxe24a-4D2YHegrqJ3TMaQ&usqp=CAU)


  ```
  1)what is ZAP?
  2)Prerequisites
  3)steps
  ```
## what is ZAP?   

Zed Attack Proxy (ZAP) is a free, open-source penetration testing tool being maintained under the umbrella of the Open Web Application Security Project (OWASP). ZAP is designed specifically for testing web applications and is both flexible and extensible.

At its core, ZAP is what is known as a “man-in-the-middle proxy.” It stands between the tester’s browser and the web application so that it can intercept and inspect messages sent between browser and web application, modify the contents if needed, and then forward those packets on to the destination. It can be used as a stand-alone application, and as a daemon process.

![Getting Started](https://www.zaproxy.org/getting-started/images/browser-no-proxy.png )

## Prerequisites
    -Java (JDK & JRE)

## Steps 
 <span style="color:white ;font-weight: bold;text-transform: uppercase;"> Step 1 : install java (jdk,jre) </span>
```
sudo apt update
sudo apt install default-jre
java -version
sudo apt install default-jdk
javac -version
```
<span style="color:white ;font-weight: bold;text-transform: uppercase;"> Step 2 : Download ZAP file </span>

<herf>https://www.zaproxy.org/download/</herf>


<span style="color:white ;font-weight: bold;text-transform: uppercase;"> Step 3 : giving execution to the ZAP.sh file
  </span>
```
chmod u+x ZAP_2_11_1_unix.sh
 ```

<span style="color:white ;font-weight: bold;text-transform: uppercase;"> Step 4 : run the zap file
  </span>
```
 sudo ./ZAP_2_11_1_unix.sh  
 ```
<span style="color:white ;font-weight: bold;text-transform: uppercase;"> Step 5: Installation Steps
  </span>

After that Installation pop-up will open

![Getting Started](./images/step-1.png)

Define the path where we want to install

![Getting Started](./images/step-2.png)

check for update

![Getting Started](./images/step-3.png)

Install the ZAP

![Getting Started](./images/step-4.png)


<span style="color:white ;font-weight: bold;text-transform: uppercase;"> Step 6 : Open the UI
  </span>

  Go to the installation path

```
cd /usr/local/bin
```

run the zap.sh file

```
./zap.sh
```
 zap will start running 
    
 ![Getting Started](./images/zap-ui.png)

<br>

<span style="color:white ;font-weight: bold;text-transform: uppercase;"> Step 7 : Click on Automated Scan 
  </span>

    Enter the url that you want to attack and click on attack for the report

   ![Getting Started](./images/automate-test.png)

<br>

<span style="color:white ;font-weight: bold;text-transform: uppercase;"> Step 8 : Output
  </span>

   ![Getting Started](./images/output.png)

   In Alert box we can see the number of vulnerabilities that are present

   ![Getting Started](./images/alert.png)

<br>

# ZAP-PROXY 

## Prerequisites
    Java (JDK & JRE)

## Steps 
 <span style="color:white ;font-weight: bold;text-transform: uppercase;"> Step 1 : install zap-proxy </span>
```
sudo apt install zaproxy 
```
<span style="color:white ;font-weight: bold;text-transform: uppercase;"> Step 2 : OWASP Zed Attack Proxy </span>
```
sudo sh zap.sh
```

<span style="color:white ;font-weight: bold;text-transform: uppercase;"> Step 3 : zap-proxy cli command</span>
```

sh ZAP.sh -daemon -quickurl https://www.clouddefense.ai/dast-dynamic-application-security-testing -quickout /tmp/myresult.xml -quickprogress

```
- quickurl = Define the target url
- quickout = Here we define output file path

![Getting Started](./images/zapproxy.png)

 
