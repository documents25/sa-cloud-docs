# EKS with fargate
  ```
  1)what is EKS?
  2)what is AWS Fargate?
  3)Prerequisites
  4)steps
  ```
## what is EKS?   

Amazon Elastic Kubernetes Service (Amazon EKS) is a managed AWS Kubernetes service that scales, manages, and deploys containerized applications. It typically runs in the Amazon public cloud, but can also be deployed on premises. 

## what is AWS Fargate?

AWS Fargate is a technology that provides on-demand, right-sized compute capacity for containers. With AWS Fargate, you don’t have to provision, configure, or scale groups of virtual machines on your own to run containers. You also don’t need to choose server types, decide when to scale your node groups, or optimize cluster packing. You can control which pods start on Fargate and how they run with Fargate profiles. Fargate profiles are defined as part of your Amazon EKS cluster.

## Prerequisites
    - AWS account with all needed permissions to create EKS cluster
    - Installed kubectl and eksctl
    - Installed AWS CLI

## Steps 
 <span style="color:white ;font-weight: bold;text-transform: uppercase;"> Step 1 : Create EKS cluster </span>
```
eksctl create cluster --name fargate-cluster --region us-east-1 
````
![Getting Started](https://miro.medium.com/max/1400/1*OcNsSR_bedD7zlclMTT2Gw.png)

<span style="color:white ;font-weight: bold;text-transform: uppercase;"> Step 2 : Check nodes & Pods using cmd  </span>
```
- kubectl get pods -A
- kubectl get nodes
```
![Getting Started](https://miro.medium.com/max/1400/1*T9b_iAYbyu_pXBmrvEH7-Q.png)

<span style="color:white ;font-weight: bold;text-transform: uppercase;"> Step 3 : Add Fargate profile to an existing EKS cluster
  </span>
```
eksctl create fargateprofile \
 --cluster fargate-cluster \
 --name play-with-fargate \
 --namespace play-with-fargate \
 --region eu-west-3
 ```

![Getting Started](https://miro.medium.com/max/1400/1*HUXoyrM0kmk6QSrYiUyM6A.png)

<span style="color:white ;font-weight: bold;text-transform: uppercase;"> Step 4 : Then check it in cloudformation stack 
  </span>

![Getting Started](https://miro.medium.com/max/1400/1*YFCxpwQqQf_j_VXEX4uRpQ.png)


you need to see in AWS Console -> Amazon Container Services, you need to be in the us-east-1 region.

![Getting Started](https://miro.medium.com/max/1400/1*KXVh-93Xr2J0XC3spdfRvA.png)

<span style="color:white ;font-weight: bold;text-transform: uppercase;"> Step 5 : deploy applications to EKS Fargate 
  </span>
<br> 
Here deploy Nginx is done to ‘play-with-fargate’ namespace: 
```
- kubectl create ns play-with-fargate
- kubectl create deployment nginx --image=nginx -n play-with-fargate
```
![Getting Started](https://miro.medium.com/max/1400/1*TUp1c_xak_TJjQYRvQHK6A.png)

We can see Nginx indeed provisioned on Fargate node

![Getting Started](https://miro.medium.com/max/1400/1*kJ8G4M1H7omXQteOIoKDTg.png)



