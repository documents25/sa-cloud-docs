
![img-1.jpg](https://speedmedia.jfrog.com/08612fe1-9391-4cf3-ac1a-6dd49c36b276/https://media.jfrog.com/wp-content/uploads/2021/12/29113553/jfrog-logo-2022.svg/mxw_64,f_auto)

# What is JFrog?
> JFrog Artifactory is the only Universal Repository Manager that supports all major packaging formats, build tools and CI servers. It is universal Artifact Repository Manager on the market, JFrog Artifactory fully supports software packages created by any language or technology. Artifactory is the only enterprise-ready repository manager available today, supporting secure, cluster, High Availability Docker registries.

![img-1.jpg](https://speedmedia.jfrog.com/08612fe1-9391-4cf3-ac1a-6dd49c36b276/https://media.jfrog.com/wp-content/uploads/2020/03/20125723/JFrog-Platform-Diagram_Mar20_Desktop.png/mxw_1080,f_auto)

# How is JFrog differ from Github?
> It’s very simple and people do confuse between these two a lot and both serves a totally different purpose from each other.

> Git is where your source code resides. Git does all the version control because it can read plain text. Git cannot handle binary files which have no textual context. Although, I don’t want to include Git LFS which is a similar kind of thing, but let’s keep things simple as of now.

> Now what to do if you want to handle your binaries? Then comes JFrog Artifactory which is the only universal solution which supports mostly all package managers available in the market. It can connect with your CI environment and do a lot more stuff than just managing your binary repositories.

> In short, Git is used for source code and Artifactory is used for the binaries which go with the source code.

# Why should use repository manager:

> Reduce number of downloads from remote repositories, this can save not only bandwidth but also time.

> Improve build stability since you are less dependent on external repositories.

> Increase performance for interaction with remote SNAPSHOT repositories.
Potential for control consumed and provided artifacts.

> Create a central storage and access to artifacts and metadata that can be used by another developer, even another projects. QA and operations team also get the benefits by using the same binary.

> Become effective platform for exchanging binary artifact within and beyond your organization without the need of building the source.

# JFrog Artifactory features:

> **Reliability**: As a local proxy to the outside world, Artifactory guarantees consistent access to the components needed by your build tools.

> **Efficiency**: Remote artifacts are cached locally for reuse, so that you don’t have to download them repeatedly.

> **Security**: Advanced security features give you control over who can access your artifacts, and where they can deploy them.

> **Stability**: Supports large load bursts with extremely high concurrency and unmatched data integrity.

> **Automation**: Automate all aspects of artifact management using a powerful REST API.

# Components to setup JFrog:

> **Oracle JDK 8 and higher**. Artifactory is Java software, so we will use Oracle Java 8 or higher to run Artifactory.

> **MySQL 5.6**. Artifactory comes with built in Derby database. It’s not a strict requirement but highly recommended to use external database. Artifactory support MySQL, PostgreSQL, Oracle and MSSQL Server. In this tutorial we’ll setup MySQL server as external database but on the same server with Artifactory. When needed in the future you can easily migrate the database to dedicated MySQL instance.

> **Nginx**. Artifactory run on top of Tomcat as application server. We can access artifactory directly, but we will use Nginx as reverse proxy for Tomcat / Artifactory. We will also setup SSL on Nginx, so SSL termination will happen on Nginx instead of on Tomcat.

# INSTALLATION

## Artifactory OSS Install in Linux Manually

> - cd /opt/
> - wget https://releases.jfrog.io/artifactory/bintray-artifactory/org/artifactory/oss/jfrog-artifactory-oss/7.21.5/jfrog-artifactory-oss-7.21.5-linux.tar.gz
> - cd artifactory-pro-7.21.5
> - cd app/bin
> - ./artifactory.sh start
> - BROWSE - http://localhost:8082
> - Username/Password - admin/password

## Artifactory Pro Install in Linux using Debian Package

> - To determine your distribution, run lsb_release -c or cat /etc/os-release
> - echo "deb https://releases.jfrog.io/artifactory/artifactory-pro-debs xenial main" | sudo tee -a /etc/apt/sources.list;
> - wget -qO - https://releases.jfrog.io/artifactory/api/gpg/key/public | sudo apt-key add -;
> - echo "deb https://releases.jfrog.io/artifactory/artifactory-pro-debs xenial main" | sudo tee -a /etc/apt/sources.list;
> - sudo apt-get update && sudo apt-get install jfrog-artifactory-pro
> - deb https://releases.jfrog.io/artifactory/artifactory-pro-debs xenial main
> - systemctl start artifactory.service
> - systemctl status artifactory.service

```
Installation directory was set to /opt/jfrog/artifactory
You can find more information in the log directory /opt/jfrog/artifactory/var/log
System configuration templates can be found under /opt/jfrog/artifactory/var/etc
Copy any configuration you want to modify from the template to /opt/jfrog/artifactory/var/etc/system.yaml
```

## Artifactory OSS Install in Linux using Docker

> - docker run -p 8081:8081 -p 8082:8082 -d releases-docker.jfrog.io/jfrog/artifactory-oss:latest
> - BROWSE - localhost:8082
> - Username/Password - admin/password

