


# Onboarding JFrog Platform - Artifactory Best Practices
This video covers an onboarding workflow that begins with adding a new user and group and then walks through the JFrog Platform configuration for creating a local Docker repository and pulling a project artifact (busybox) to the Docker repository. Lastly the Administrator is introduced to Permission Targets for managing resources permissions across the JFrog Platform .

There are many topics that are not covered in this onboarding journey, simply to keep it quick and to guide you to specific resources for expanding the number of package types, registries, repositories, or products to address your environment.


## 1. Repository design characteristics
One of the fundamental design characteristics for your universal binary repository structure is the placement of local, remote and virtual repositories. 

*Repository Types*

Local repositories are physical and locally managed, while remote repositories are caching proxies of a remote server. To improve developer efficiency, remote repositories will attempt to resolve the artifact from the remote server and then store that artifact in the local cache for future requests. Virtual repositories simplify search and build processes by aggregating local and remote repositories under a single  URL. Once set up, some customers prefer to only have virtual repositories visible to developers and projects.

*Package Type Mirrors Repository Type*

Within JFrog Platform there are over 25 different package types available to integrate into your development environment. Each repository is constructed for one defined package type. For example, for Maven package integration, create a Maven  repository structure. When adding another package type, for example Docker, then create a separate Docker set of repositories to mirror your development lifecycle, and repeat to support all the development environments across your organization. The one exception is the package type Generic, which can accept any file format.

![img-1.jpg](https://www.jfrog.com/confluence/download/attachments/114761990/JFrog%20Platform%20-%20Package%20Types.png?version=1&modificationDate=1602662823000&api=v2)

## 2. Naming conventions and structure
JFrog recommends a 4 part naming convention to scale and manage your global repositories. As shown, this includes:

*<team>-<technology>-<maturity>-<locator>*

A product or team name as the primary identifier of the project.
The technology, tool or package type being used.
The package maturity level, such as the development, staging and release stages.
The locator, the physical topology of your artifacts. 
Creating Artifactory repositories per team, or folders inside repositories, enables each team to handle their specific vulnerabilities.

![img-2.jpg](https://www.jfrog.com/confluence/download/attachments/114761990/4-part%20naming%20convention.png?version=2&modificationDate=1602612541000&api=v2)


See the complete whitepaper, Best Practices for Structuring and Naming Artifactory Repositories, for further descriptions of the use cases and configurations to optimize your development environment.

## 3. Create a repository structure that mirrors your development life cycle.
*Repository Structure*

Create a repository structure that mirrors your development life cycle in order to support an intuitive promotion of the artifact across different stages in the CI. A classic 4 stage development life cycle such as; development, test, staging and production would have 4 repositories:

yourcompany-docker-dev-local

yourcompany-docker-test-local

yourcompany-docker-stage-local

yourcompany-docker-prod-local

![img-3.jpg](https://www.jfrog.com/confluence/download/attachments/114761990/JFrog%20Platform%20-%20Repository%20Browser.png?version=1modificationDate=1602612696000&api=v2)

Publish Build Artifacts with Tags/Metadata to Each Stage of the Development Lifecycle

Instead of deploying artifacts into a quarantined area, JFrog Artifactory simply tags artifacts with metadata.

![img-4.jpg](https://www.jfrog.com/confluence/download/attachments/114761990/Publish%20Build%20Artifacts%20with%20Tags_Metadata.png?version=1&modificationDate=1602612855000&api=v2)

With promotion properties in place, you can set permissions that apply to artifacts tagged with properties that represent different stages. We can automate this using dev tool integrations as we deploy from one stage to another.

Then the artifacts will be published to a repository that grants access to users with roles relevant to that stage, like testers for example.

Using REST API, you can automate many of the tasks involved in build promotions.
