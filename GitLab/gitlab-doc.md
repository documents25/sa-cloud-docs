> ![Getting Started](https://www.gartner.com/imagesrv/peer-insights/vendors/logos/gitlab.png)

# **Gitlab Best Practices**

 ### 1. Use feature branches rather than direct commits on the main branch.
``` 
Using feature branches is a simple way to develop and keep the source code clean. If a team has recently transitioned to Git from SVN, for example, they’ll be used to a trunk-based workflow. When using Git, developers should create a branch for anything they’re working on so that contributors can easily start the code review process before merging.
```

### 2. Test all commits, not only ones on the main branch.
```
Some developers set up their CI to only test what has been merged into the main branch, but this is too late in the software development lifecyle, and everyone - from developers to product managers - should feel feel confident that the main branch always has green tests. It’s inefficient for developers to have to test main before they start developing new features.
```

 ### 3. Run every test on all commits. (If tests run longer than 5 minutes, they can run in parallel.)
```
When working on a feature branch and adding new commits, run tests right away. If the tests are taking a long time, try running them in parallel. Do this server-side in merge requests, running the complete test suite. If there is a test suite for development and another only for new versions, it’s worthwhile to set up [parallel] tests and run them all.
```

### 4. Perform code reviews before merging into the main branch.
```
Don’t test everything at the end of a week or project. Code reviews should take place as soon as possible, because developers are more likely to identify issues that could cause problems later in the lifecycle. Since they’ll find problems earlier, they’ll have an easier time creating solutions.
```

### 5. Deployments are automatic based on branches or tags.
```
If developers don’t want to deploy main every time, they can create a production branch. Rather than using a script or doing it manually, teams can use automation or have a specific branch that triggers a production deploy.
```

### 6. Tags are set by the user, not by CI.
```
Developers should use tags so that the CI will perform an action rather than having the CI change the repository. If teams require detailed metrics, they should have a server report detailing new versions.
```

### 7. Releases are based on tags.
```
Each tag should create a new release. This practice ensures a clean, efficient development environment.
```

### 8. Pushed commits are never rebased.
```
When pushing to a public branch, developers shouldn’t rebase it, because that makes it difficult to identify the improvement and test results, while cherry picking. Sometimes this tip can be ignored when asking someone to squash and rebase at the end of a code review process to make something easier to revert. However, in general, the guideline is: Code should be clean, and history should be realistic.
```

### 9. Everyone starts from main and targets main.
```
This tip prevents long branches. Developers check out main, build a feature, create a merge request, and target main again. They should do a complete review before merging and eliminating any intermediate stages.
```

### 10. Fix bugs in main first and release branches second.
```
After identifying a bug, a problematic action someone could take is fix it in the just-released version and not fix it in main. To avoid it, developers should always fix forward by pushing the change in main, then cherry-pick it into another patch-release branch.
```

### 11. Commit messages reflect intent.
```
Developers should not only say what they did, but also why they did it. An even more useful tactic is to explain why this option was selected over others to help future contributors understand the development process. Writing descriptive commit messages is useful for code reviews and future development.
```

## **Continuous integration best practices**

- ### **CI best practice: Commit early, commit often:-**

  It’s much easier to fix small problems than big problems, as a general rule. One of the biggest advantages of continuous integration is that code is integrated into a shared repository against other changes happening at the same time. If a development team commits code changes early and often, bugs are easier to identify because there is less code to sort through.

- ### **CI best practice: Read the documentation (and then read it again):-**

  Continuous integration systems make documentation widely available, and this documentation can be very helpful long after you’ve implemented CI into your workflow. At GitLab, we have thorough CI/CD documentation that is updated frequently to reflect the latest processes.

  In can be helpful to reference the documentation in READMEs or in other accessible formats. Encourage team members to read the documentation first, bookmark links, create FAQs, and incorporate these resources into onboarding for new team members.

- ### **CI best practice: Optimize pipeline stages:-**
  
  CI pipelines contain jobs and stages: Jobs are the activities that happen within a particular stage, and once all jobs pass, code moves to the next stage. To get the most out of your CI pipelines, optimize stages so that failures are easy to identify and fix.

  Stages are an easy way to organize similar jobs, but there may be a few jobs in your pipeline that could safely run in an earlier stage without negatively impacting your project if they fail. Consider running these jobs in an earlier stage to speed up CI pipelines.

- ### **CI best practice: Make builds fast and simple:-**

  Nothing slows down a pipeline like complexity. Focus on keeping builds fast, and the best way to do that is by keeping things as simple as possible.

  Every minute taken off build times is a minute saved for each developer every time they commit. Since CI demands frequent commits, this time can add up. Martin Fowler discusses a guideline of the ten-minute build that most modern projects can achieve. Since continuous integration demands frequent commits, saving time on commit builds can give developers a lot of time back.

- ### **CI best practice: Use failures to improve processes:-**
 
  Improvement is a process. When teams change their response to failures, it creates a cultural shift for continuous improvement. Instead of asking who caused the failure, ask what caused the failure. This means shifting from a blaming culture to a learning culture.

  If teams are doing frequent commits, it becomes much easier to identify problems and solve them. If there are patterns in failed builds, look at the underlying causes. Are there non-code errors that are causing builds unnecessarily? Maybe incorporate an allow_failure parameter. Look for ways to continually improve, make failures blameless, and look for causes (not culprits).

- ### **CI best practice: Test environment should mirror production:-**

  In continuous integration, every commit triggers a build. These builds then run tests to identify if something will be broken by the code changes you introduce. The test pyramid is a way for developers to think of how to balance testing. End-to end testing is mostly used as a safeguard, with unit testing being used most often to identify errors. One important thing to keep in mind with testing is the environment. When the testing and production environments match, it means that developers can rely on the results and deploy with confidence.

  In GitLab, Review Apps put the new code into a production-like live environment to visualize code changes. This feature helps developers assess the impact of changes.

  Continuous integration helps developers deploy faster and get feedback sooner. Ultimately, the best continuous integration system is the one you actually use. Find the right CI for your needs and then incorporate these best practices to make the most of your new CI workflow.

## **Identify a branching strategy**
- Software development teams include professionals with diverse experiences and background, which can potentially cause conflicting workflows. Determining a single branching strategy is the solution to a chaotic development experience.

- While there are several approaches to development, the most common are:
  1. Centralized workflow: Teams use only a single repository and commit directly to the main branch.
  2. Feature branching: Teams use a new branch for each feature and don’t commit directly to the main branch.
  3. GitFlow: An extreme version of feature branching in which development occurs on the develop branch, moves to a release branch, and merges into the main branch.
  4. Personal branching: Similar to feature branching, but rather than develop on a branch per feature, it’s per developer. Every user merges to the main branch when they complete their work.

## **Develop using branches**
- Using branches, software development teams can make changes without affecting the main codeline. The running history of changes are tracked in a branch, and when the code is ready, it’s merged into the main branch.

- Branching organizes development and separates work in progress from stable, tested code in the main branch. Developing in branches ensures that bugs and vulnerabilities don’t work their way into the source code and impact users, since testing and finding those in a branch is easier.

## <span>**GitLab security best practices:**</span>

Here is a list of things you can do to secure your GitLab accounts from attacks:

   ### **Require multi-factor authentication for group members:**
     Ensuring only those allowed to access your repositories in GitLab have access to them is half the battle. This is best done with multi-factor authentication (MFA) or two-factor authentication (2FA). It requires any user attempting to log in to also receive an access code on their mobile device or email. This is table stakes in security today, and is essential for securing GitLab.
    
   ### **Restrict IP access:**
     This is a network-level security measure. It is not new, but is often overlooked. IP addresses are still a great way to track where your users come from. In today’s world of remote work this is more of a challenge as the number of IPs accessing your resources has grown exponentially. Still, by noticing patterns of daily access, you can use systems to easily spot suspicious new IPs that access your systems from strange locations. An extra step to whitelist IP addresses of all employee devices will greatly improve this security measure. It is important to not block off access to genuine users while doing this.
   ### **Disallow forking of project:**
    We now move to the application-level security measures. Forking is easy to do in GitLab and can result in code getting around to places that you have no control over. While the basic GitLab tier doesn’t restrict forking, the Premium or Silver tiers do enable you to block forking of a project outside that group. This gives you more control over your source code. 
   ### **Enable Secret Detection:**
    Many attacks, even the ones listed above, were successful because someone carelessly left a secret token or password out in the open somewhere. GitLab has a built-in feature to check for accidentally exposed secret information anywhere in your repositories. Activate and use this capability, and it will prevent an inevitable attack in the future. 
   ### **Disallow unsecured URLs in webhook:**
    Webhooks are unavoidable in modern cloud operations, and they contain sensitive information in the form of payload. To secure webhooks it is essential to use SSL verification (HTTPS) so that the data sent is delivered only to the intended endpoint and no man-in-the-middle has access to the payload.

## **Migrate projects to a GitLab instance all tiers**

    See these documents to migrate to GitLab:

   - <a href="https://docs.gitlab.com/ee/user/project/import/bitbucket.html">From Bitbucket Cloud</a>
   - <a href="https://docs.gitlab.com/ee/user/project/import/bitbucket_server.html">From Bitbucket Server (also known as Stash)</a>
   - <a href="https://docs.gitlab.com/ee/user/project/import/clearcase.html">From ClearCase</a>
   - <a href="https://docs.gitlab.com/ee/user/project/import/cvs.html">From CVS</a>
   - <a href="https://docs.gitlab.com/ee/user/project/import/fogbugz.html">From FogBugz</a>
   - <a href="https://docs.gitlab.com/ee/user/project/import/github.html">From GitHub.com or GitHub Enterprise</a>
   - <a href="https://docs.gitlab.com/ee/user/project/import/gitlab_com.html">From GitLab.com</a>
   - <a href="https://docs.gitlab.com/ee/user/project/import/gitea.html">From Gitea</a>
   - <a href="https://docs.gitlab.com/ee/user/project/import/perforce.html">From Perforce</a>
   - <a href="https://docs.gitlab.com/ee/user/project/import/svn.html">From SVN</a>
   - <a href="https://docs.gitlab.com/ee/user/project/import/tfvc.html">From TFVC</a>
   - <a href="https://docs.gitlab.com/ee/user/project/import/repo_by_url.html">From repository by URL</a>
   - <a href="https://docs.gitlab.com/ee/user/project/import/manifest.html">By uploading a manifest file (AOSP)</a>
   - <a href="https://docs.gitlab.com/ee/user/project/import/phabricator.html">From Phabricator</a>
   - <a href="https://docs.gitlab.com/ee/user/project/import/jira.html">From Jira (issues only)</a>

   ## **Project import history**

You can view all project imports created by you. This list includes the following:

   - Source (without credentials for security reasons)
   - Destination
   - Status
   - Error details if the import failed

### **To view project import history:**

   - Sign in to GitLab.
   - On the top bar, select New ( + ).
   - Select New project/repository.
   - Select Import project.
   - Select History.

   > ![Getting Started](https://docs.gitlab.com/ee/user/project/import/img/gitlab_import_history_page_v14_10.png)

  ## **Automate group and project import premium**

    The GitLab Professional Services team uses Congregate to orchestrate user, group, and project import API calls. With Congregate, you can migrate data to GitLab from:

    - Other GitLab instances
    - GitHub Enterprise
    - GitHub.com
    - Bitbucket Server
    - Bitbucket Data Center

## **Gitlab CI/CD Pipeline**

### **Create a .gitlab-ci.yml file**

    The .gitlab-ci.yml file is a YAML file where you configure specific instructions for GitLab CI/CD.

In this file, you define:

    The structure and order of jobs that the runner should execute.
    
    The decisions the runner should make when specific conditions are encountered.

For example, you might want to run a suite of tests when you commit to any branch except the default branch. When you commit to the default branch, you want to run the same suite, but also publish your application.

All of this is defined in the .gitlab-ci.yml file.

To create a .gitlab-ci.yml file:

   1. On the left sidebar, select **Project information > Details**.

   2. Above the file list, select the branch you want to commit to, select the plus icon, then select **New file**:
   >![Selection of file](https://docs.gitlab.com/ee/ci/quick_start/img/new_file_v13_6.png)

   3. For the Filename, type .gitlab-ci.yml and in the larger window, paste this sample code:

    build-job:
      stage: build
      script:
        - echo "Hello, $GITLAB_USER_LOGIN!"

    test-job1:
      stage: test
      script:
        - echo "This job tests something"

    test-job2:
      stage: test
      script:
        - echo "This job tests something, but takes more time than test-job1."
        - echo "After the echo commands complete, it runs the sleep command for 20 seconds"
        - echo "which simulates a test that runs 20 seconds longer than test-job1"
        - sleep 20

    deploy-prod:
      stage: deploy
      script:
        - echo "This job deploys something from the $CI_COMMIT_BRANCH branch."
      
**$GITLAB_USER_LOGIN** and **$CI_COMMIT_BRANCH** are <a href="https://docs.gitlab.com/ee/ci/variables/predefined_variables.html" >predefined variables</a> that populate when the job runs.

   4.  Select **Commit changes**.

## **.gitlab-ci.yml** tips:

- After you create your first .gitlab-ci.yml file, use the pipeline editor for all future edits to the file. With the pipeline editor, you can:

      - Edit the pipeline configuration with automatic syntax highlighting and validation.
      
      - View the CI/CD configuration visualization, a graphical representation of your .gitlab-ci.yml file.

- If you want the runner to use a Docker container to run the jobs, edit the .gitlab-ci.yml file to include an image name:

      default:
        image: ruby:2.7.5

This command tells the runner to use a Ruby image from Docker Hub and to run the jobs in a container that’s generated from the image.

This process is different than building an application as a Docker container. Your application does not need to be built as a Docker container to run CI/CD jobs in Docker containers.

- Each job contains scripts and stages:

      The default keyword is for custom defaults, for example with before_script and after_script.
    
      stage describes the sequential execution of jobs. Jobs in a single stage run in parallel as long as there are available runners.
    
      Use Directed Acyclic Graphs (DAG) keywords to run jobs out of stage order.

- You can set additional configuration to customize how your jobs and stages perform:

      Use the rules keyword to specify when to run or skip jobs. The only and except legacy keywords are still supported, but can’t be used with rules in the same job.
    
      Keep information across jobs and stages persistent in a pipeline with cache and artifacts. These keywords are ways to store dependencies and job output, even when using ephemeral runners for each job.

- For the complete .gitlab-ci.yml syntax, see the full .gitlab-ci.yml reference topic.

## **View the status of your pipeline and jobs**

When you committed your changes, a pipeline started.

### **To view your pipeline:**

    Go to CI/CD > Pipelines.
   A pipeline with three stages should be displayed:
  >![running pipeline image](https://docs.gitlab.com/ee/ci/quick_start/img/three_stages_v13_6.png)

    To view a visual representation of your pipeline, select the pipeline ID.
  >![visual representation of pipeline](https://docs.gitlab.com/ee/ci/quick_start/img/pipeline_graph_v13_6.png)

    To view details of a job, select the job name, for example, deploy-prod.
  >![details of job](https://docs.gitlab.com/ee/ci/quick_start/img/job_details_v13_6.png)

    NOTE: If the job status is stuck, check to ensure a runner is properly configured for the project.