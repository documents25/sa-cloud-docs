# GitLab Docs

## **Welcome to GitLab documentation.**
```
Here you can access the complete documentation for GitLab, the single application for the entire DevOps lifecycle.
```

# **What is GIT ?**

```
Git is a version control system used to track changes in computer files. Git's primary purpose is to manage any changes made in one or more projects over a given period of time. It helps coordinate work among members of a project team and tracks progress over time. Git also helps both programming professionals and non-technical users by monitoring their project files. 
    
Git can handle projects of any size. It allows multiple users to work together without affecting each other's work.
```

# **GIT Installation and Configuration**

### **Installing on Linux**
```yaml
#for Fedora or any closely-related RPM-based distribution, such as RHEL or CentOS

sudo dnf install git-all 

#for Debian-based distribution, such as Ubuntu

sudo apt install git-all
```
### **Installing on MacOs**
```yaml
#Install homebrew if you don't already have it, then:

brew install git
```
### **OR**

```yaml
#Install MacPorts if you don't already have it, then:

sudo port install git
```
### **Installing on Windows**

**Standalone Installer**

<a href="https://github.com/git-for-windows/git/releases/download/v2.37.0.windows.1/Git-2.37.0-32-bit.exe"> 32-bit Git for Windows Setup</a>

<a href="https://github.com/git-for-windows/git/releases/download/v2.37.0.windows.1/Git-2.37.0-64-bit.exe">64-bit Git for Windows Setup</a>

**Portable ("thumbdrive edition")**

<a href="https://github.com/git-for-windows/git/releases/download/v2.37.0.windows.1/PortableGit-2.37.0-32-bit.7z.exe">32-bit Git for Windows Portable</a>

<a href="https://github.com/git-for-windows/git/releases/download/v2.37.0.windows.1/PortableGit-2.37.0-64-bit.7z.exe">64-bit Git for Windows Portable</a>

**Using winget tool**

Install <a href="https://docs.microsoft.com/en-us/windows/package-manager/winget">winget tool</a> if you don't already have it, then type this command in command prompt or Powershell.

```
winget install --id Git.Git -e --source winget 
```


# **What is GitLab ?**

```
GitLab is the first single application for software development, security, and operations that enables Concurrent DevOps. GitLab makes the software lifecycle faster and radically improves the speed of business.

GitLab is a web-based Git repository that provides free open and private repositories, issue-following capabilities, and wikis. It is a complete DevOps platform that enables professionals to perform all the tasks in a project—from project planning and source code management to monitoring and security. Additionally, it allows teams to collaborate and build better software. 

GitLab helps teams reduce product lifecycles and increase productivity, which in turn creates value for customers. The application doesn't require users to manage authorizations for each tool. If permissions are set once, then everyone in the organisation has access to every component.

Customers can opt for the paid version of GitLab if they want to access more functionalities. For example, the Premium version costs $19 per user/month.
```

# **Why use Gitlab ?** 

```
The main benefit of using GitLab is that it allows all the team members to collaborate in every phase of the project. GitLab offers tracking from planning to creation to help developers automate the entire DevOps lifecycle and achieve the best possible results. More and more developers have started to use GitLab because of its wide assortment of features and brick blocks of code availability.
```

# Gitlab Installation And Configuration

## **We can use Gitlab in two ways:**
```
1. SAAS Platform
2. Self Managed
```
Note: In SAAS platform, we do not need to install gitlab, we just need to install git in our system and we are good to go.

### **For self managed Gitlab:-**
### **GitLab installation minimum requirements:**

### **Operating Systems :**

#### **Supported Linux distributions**
```
GitLab officially supports LTS versions of operating systems. While OSs like Ubuntu have a clear distinction between LTS and non-LTS versions, there are other OSs, openSUSE for example, that don’t follow the LTS concept. Hence to avoid confusion, the official policy is that at any point of time, all the operating systems supported by GitLab are listed in the installation page.
```

```
| OS Version      | End Of Life  | Last supported GitLab version |     
|-----------------|--------------|-------------------------------|
| Raspbian Wheezy | May 2015     | GitLab CE 8.17                |   
| OpenSUSE 13.2   | January 2017 | GitLab CE / GitLab EE 9.1     |   
| Ubuntu 12.04    | April 2017   | GitLab CE / GitLab EE 9.1     |   
| OpenSUSE 42.1   | May 2017     | GitLab CE / GitLab EE 9.3     |   
| OpenSUSE 42.2   | January 2018 | GitLab CE / GitLab EE 10.4    |  
| Debian Wheezy   | May 2018     | GitLab CE / GitLab EE 11.6    |  
| Raspbian Jessie | May 2017     | GitLab CE 11.7                |  
| Ubuntu 14.04    | April 2019   | GitLab CE / GitLab EE 11.10   |   
```

 
## **Unsupported Linux distributions and Unix-like operating systems**
```
① Arch Linux
② Fedora
③ FreeBSD
④ Gentoo
⑤ macOS
```


## **Install GitLab on cloud providers**
Regardless of the installation method, you can install GitLab on several cloud providers, assuming the cloud provider supports it. Here are several possible installation methods, the majority which use the Linux packages:

```
| Cloud provider              | Description                                                        |                
|-----------------------------|-------------------------------------------------------------------
| AWS (HA)                    | Install GitLab on AWS using the community AMIs provided by GitLab. |    
| Google Cloud Platform (GCP) | Install GitLab on a VM in GCP.                                     |   
| Azure                       | Install GitLab from Azure Marketplace.                  
```

# **What is Gitlab runner?**

> ![Getting Started](https://i0.wp.com/techdirectarchive.com/wp-content/uploads/2021/09/GitLabrunner.jpg?ssl=1)

```
GitLab Runner is an application that works with GitLab CI/CD to run jobs in a pipeline.

You can choose to install the GitLab Runner application on infrastructure that you own or manage. If you do, you should install GitLab Runner on a machine that’s separate from the one that hosts the GitLab instance for security and performance reasons. When you use separate machines, you can have different operating systems and tools, like Kubernetes or Docker, on each.

GitLab Runner is open-source and written in Go. It can be run as a single binary;no language-specific requirements are needed.

You can install GitLab Runner on several different supported operating systems. Other operating systems may also work, as long as you can compile a Go binary on them.

GitLab Runner can also run inside a Docker container or be deployed into a Kubernetes cluster.
```
# <span> Gitlab runner installation methods in windows, linux and macos</span>

**Different types of Runners**
- There are different types of GitLab runners and they are as follows:-

① Shared Runner

② Specific Runner

③ Group Runner


## **Shared Runner** 

    Shared Runners are relevant for jobs that have similar requirements, and multiple projects. Rather than having multiple Runners idling for many projects, you can have a single or a small number of Runners that handle multiple projects. This makes it easier to maintain and update them. Shared Runners process jobs using a fair usage queue. In contrast to specific Runners that use a FIFO queue, this prevents cases where projects create hundreds of jobs which can lead to eating all available shared Runners resources. A Runner that serves all projects is called a shared Runner.

### Enable shared runners for a project

    On GitLab.com, shared runners are enabled in all projects by default.

    On self-managed instances of GitLab, an administrator can enable them for all new projects.

For existing projects, an administrator must install and register them.

To enable shared runners for a project:

    Go to the project’s Settings > CI/CD and expand the Runners section.
    Select Enable shared runners for this project.

Enable shared runners for a group

To enable shared runners for a group:

    Go to the group’s Settings > CI/CD and expand the Runners section.
    
    Select Enable shared runners for this group.

### Disable shared runners for a project

    You can disable shared runners for individual projects or for groups. You must have the Owner role for the project or group.

To disable shared runners for a project:

    Go to the project’s Settings > CI/CD and expand the Runners section.
    
    In the Shared runners area, select Enable shared runners for this project so the toggle is grayed-out.

Shared runners are automatically disabled for a project:

    If the shared runners setting for the parent group is disabled, and
    If overriding this setting is not permitted at the project level.

Disable shared runners for a group

To disable shared runners for a group:

    Go to the group’s Settings > CI/CD and expand the Runners section.
    
    In the Shared runners area, turn off the Enable shared runners for this group toggle.
    
    Optionally, to allow shared runners to be enabled for individual projects or subgroups, select Allow projects and subgroups to override the group setting.

## **Specific Runner** 

    Specific Runners are useful for jobs that have special requirements or for projects with a specific demand. If a job has certain requirements, you can setup the specific Runner with this in mind, while not having to do this for all Runners. For example, if you want to deploy a certain project, you can setup a specific Runner to have the right credentials for this. The usage of tags may be useful in this case. Specific Runners process jobs using a FIFO queue.

### Create a specific runner

You can create a specific runner for your self-managed GitLab instance or for GitLab.com.

Prerequisite:

    You must have at least the Maintainer role for the project.

To create a specific runner:

    Install GitLab Runner.
    
    On the top bar, select Menu > Projects and find the 
    project where you want to use the runner.
    
    On the left sidebar, select Settings > CI/CD.
    
    Expand Runners.
    
    In the Specific runners section, note the URL and token.
    
    Register the runner.

The runner is now enabled for the project.

### Enable a specific runner for a different project

    After a specific runner is created, you can enable it for other projects.

Prerequisites: You must have at least the Maintainer role for:

    The project where the runner is already enabled.
    
    The project where you want to enable the runner.
    
    The specific runner must  be locked.

To enable a specific runner for a project:

    On the top bar, select Menu > Projects and find the project where you want to enable the runner.
    
    On the left sidebar, select Settings > CI/CD.
    Expand General pipelines.
    
    Expand Runners.
    
    By the runner you want, select Enable for this project.

You can edit a specific runner from any of the projects it’s enabled for. The modifications, which include unlocking and editing tags and the description, affect all projects that use the runner.

### Prevent a specific runner from being enabled for other projects

    You can configure a specific runner so it is “locked” and cannot be enabled for other projects. This setting can be enabled when you first register a runner, but can also be changed later.

To lock or unlock a specific runner:

    Go to the project’s Settings > CI/CD.
    
    Expand the Runners section.
    
    Find the specific runner you want to lock or unlock. 
    
    Make sure it’s enabled. You cannot lock shared or group runners.
    
    Select Edit.
    
    Check the Lock to current projects option.
    
    Select Save changes.

## **Group Runner** 

    Group Runners are useful when you have multiple projects under one group and would like all projects to have access to a set of Runners. Group Runners process jobs using a FIFO queue.

### Create a group runner

Introduced in GitLab 14.10, path changed from Settings > CI/CD > Runners.

You can create a group runner for your self-managed GitLab instance or for GitLab.com. You must have the Owner role for the group.

To create a group runner:

    Install GitLab Runner.

    Go to the group you want to make the runner work for.

    On the left sidebar, select CI/CD > Runners.

    Note the URL and token.

    Register the runner.

### View and manage group runners

You can view and manage all runners for a group, its subgroups, and projects. You can do this for your self-managed GitLab instance or for GitLab.com. You must have the Owner role for the group.

    Go to the group where you want to view the runners.

    On the left sidebar, select CI/CD > Runners.

    The following fields are displayed.

### Pause or remove a group runner

You can pause or remove a group runner for your self-managed GitLab instance or for GitLab.com. You must have the Owner role for the group.

    Go to the group you want to remove or pause the runner for.
    
    On the left sidebar, select CI/CD > Runners.
    
    Select Pause or Remove runner.
    
        - If you pause a group runner that is used by multiple projects, the runner pauses for all projects.
        
        - From the group view, you cannot remove a runner that is assigned to more than one project. You must remove it from each project first.
    
    On the confirmation dialog, select OK
<br>

## **Executors of Gitlab Runner**
GitLab Runner implements a number of executors that can be used to run your builds in different scenarios.

   - SSH
   - Shell
   - Parallels
   - VirtualBox
   - Docker
   - Docker Machine (auto-scaling)
   - Kubernetes
   - Custom

## **Selecting the executor**

The executors support different platforms and methodologies for building a project. The table below shows the key facts for each executor which will help you decide which executor to use.

> ![Selecting The Executor](https://gitlab.com/documents25/sa-cloud-docs/-/raw/stage/GitLab-Doc/Images/selecting_the_executor.png)

### **Shell executor**

    Shell is the simplest executor to configure. All required dependencies for your builds need to be installed manually on the same machine that GitLab Runner is installed on.

### **Virtual Machine executor (VirtualBox / Parallels)**

    This type of executor allows you to use an already created virtual machine, which is cloned and used to run your build. We offer two full system virtualization options: VirtualBox and Parallels. They can prove useful if you want to run your builds on different operating systems, since it allows the creation of virtual machines on Windows, Linux, macOS or FreeBSD, then GitLab Runner connects to the virtual machine and runs the build on it. Its usage can also be useful for reducing infrastructure costs.

### **Docker executor**

    A great option is to use Docker as it allows a clean build environment, with easy dependency management (all dependencies for building the project can be put in the Docker image). The Docker executor allows you to easily create a build environment with dependent services, like MySQL.

### **Docker Machine executor**

    The Docker Machine is a special version of the Docker executor with support for auto-scaling. It works like the normal Docker executor but with build hosts created on demand by Docker Machine.

### **Kubernetes executor**

    The Kubernetes executor allows you to use an existing Kubernetes cluster for your builds. The executor will call the Kubernetes cluster API and create a new Pod (with a build container and services containers) for each GitLab CI job.

### **SSH executor**

    The SSH executor is added for completeness, but it’s the least supported among all executors. It makes GitLab Runner connect to an external server and runs the builds there. We have some success stories from organizations using this executor, but usually we recommend using one of the other types.

### **Custom executor**

    The Custom executor allows you to specify your own execution environments. When GitLab Runner does not provide an executor (for example, LXC containers), you are able to provide your own executables to GitLab Runner to provision and clean up any environment you want to use.

## **Compatibility chart**

Supported features by different executors:
> ![Compatibility chart of gitlab runner](https://gitlab.com/documents25/sa-cloud-docs/-/raw/stage/GitLab-Doc/Images/Compatibility_chart_of_gitlab_runner.png)

## <span>Install GitLab Runner on Linux</span>

**Download and install binary**

① Download the binary for your system
- sudo curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64

② Give it permission to execute
- sudo chmod +x /usr/local/bin/gitlab-runner

③ Create a GitLab Runner user
- sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash

④ Install and run as a service
- sudo gitlab-runner install --user=gitlab-runner   --working-directory=/home/gitlab-runner
- sudo gitlab-runner start

**Command to register runner**
- sudo gitlab-runner register --url https://gitlab.com/ --registration-token $REGISTRATION_TOKEN

## <span>Install GitLab Runner on MacOS</span>

**Download and install binary**

① Download the binary for your system
- sudo curl --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-darwin-amd64

② Give it permission to execute
- sudo chmod +x /usr/local/bin/gitlab-runner

③ The rest of the commands execute as the user who will run the runner

④ Register the runner (steps below), then run
- cd ~
- gitlab-runner install
- gitlab-runner start

**Command to register runner**
- gitlab-runner register --url https://gitlab.com/ --registration-token $REGISTRATION_TOKEN

## <span>Install GitLab Runner on Windows</span>

① Create a folder somewhere in your system, For instance on the C drive. This can be created on any other drive: C:\GitLab-Runner.

 > ![Getting Started](https://i0.wp.com/techdirectarchive.com/wp-content/uploads/2021/09/Screenshot-2021-09-27-at-14.16.28.jpg?ssl=1)  

② Download the binary for 64-bit or 32-bit and put it into the folder you created.
– Upon clicking on the 64-bits, the GitLab runner executable will be downloaded as shown below.

 > ![Getting Started](https://i0.wp.com/techdirectarchive.com/wp-content/uploads/2021/09/Screenshot-2021-09-27-at-14.22.26.jpg?ssl=1)  

③ Copy the GitLab runner executable that has just been downloaded and paste it in the location that was created.
– The following assumes you have renamed the binary to GitLab-runner.exe (This step is optional).

 > ![Getting Started](https://i0.wp.com/techdirectarchive.com/wp-content/uploads/2021/09/Screenshot-2021-09-27-at-14.26.45.jpg?resize=1024%2C595&ssl=1)  

 > ![Getting Started](https://i0.wp.com/techdirectarchive.com/wp-content/uploads/2021/09/Screenshot-2021-09-27-at-14.22.26.jpg?resize=768%2C347&ssl=1)  

- Note: Make sure to restrict the Write permissions on the GitLab Runner directory and executable. If you do not set these permissions, regular users can replace the executable with their own and run arbitrary code with elevated privileges. You can verify this by following the steps below.
- You can right click on the GitLab Runner directory, click on Properties and then Security
- Ensure the regular users do not have the Write Permission assigned.

④ Run an elevated command prompt as shown below

 > ![Getting Started](https://i0.wp.com/techdirectarchive.com/wp-content/uploads/2021/09/Screenshot-2021-09-27-at-23.37.13.jpg?ssl=1) 

This will ensure the Command Prompt opens as an Administrator as shown below.
- Navigate to the GitLab-Runner directory

> ![Getting Started](https://i0.wp.com/techdirectarchive.com/wp-content/uploads/2021/09/Screenshot-2021-09-27-at-23.41.29.jpg?resize=768%2C202&ssl=1) 

- Run service using Built-in System Account: Now, we have to install the GitLab Runner as a service using the command below.

``` bash
gitlab-runner.exe install
``` 
> ![Getting Started](https://i0.wp.com/techdirectarchive.com/wp-content/uploads/2021/09/Screenshot-2021-09-27-at-23.46.58.jpg?resize=768%2C234&ssl=1)

- If you wish to run service using user account, please use the command below and substitute your account details appropirately.

``` bash
gitlab-runner.exe install --user ENTER-YOUR-USERNAME --password ENTER-YOUR-PASSWORD
```

- Note: When you run .\gitlab-runner.exe install it installs gitlab-runner as a Windows service. You can find the logs in the Event Viewer with the provider name gitlab-runner. You can also view this from PowerShell using the command below.


<br>

## <span> View the version of GitLab- Runner Installed </span>

> ![Getting Started](https://i0.wp.com/techdirectarchive.com/wp-content/uploads/2021/09/Screenshot-2021-09-28-at-00.04.49.jpg?resize=768%2C410&ssl=1)

- Start the GitLab Runner Service: To do this, use the command below. From the image below, the installed GitLab-Runner is already running.

``` bash
gitlab-runner.exe start
```
> ![Getting Started](https://i0.wp.com/techdirectarchive.com/wp-content/uploads/2021/09/Screenshot-2021-09-28-at-00.15.34.jpg?resize=768%2C484&ssl=1)


## <span>Register GitLab Runner</span>

```
Registering a runner is the process that binds the runner with one or more GitLab instances. After you install the application, you register individual runners, or multiple runners on the same host machine, each with a different configuration, by repeating the register command. Runners are the agents that run the CI/CD jobs that come from GitLab.

```

Run the command below to Register GitLab-Runner:-
``` bash
gitlab-runner.exe register
```
Next, you will be required to enter your GitLab instance URL (also known as the gitlab-ci coordinator URL) as shown below

> ![Getting Started](https://i0.wp.com/techdirectarchive.com/wp-content/uploads/2021/09/Screenshot-2021-09-28-at-00.41.23.jpg?resize=1024%2C300&ssl=1)

> ![Getting Started](https://i0.wp.com/techdirectarchive.com/wp-content/uploads/2021/09/Screenshot-2021-09-28-at-00.43.08.jpg?resize=1024%2C260&ssl=1)

# <span>Setting up a specific GitLab-Runner</span>
- Enter the token you obtained to register the runner.
- To get your Token, you will need to access your GitLab project as discussed above

> ![Getting Started](https://i0.wp.com/techdirectarchive.com/wp-content/uploads/2021/09/Screenshot-2021-09-28-at-00.49.54.jpg?w=579&ssl=1)

- Naviagte to the following section “Runners” and Expand it as shown below

> ![Getting Started](https://i0.wp.com/techdirectarchive.com/wp-content/uploads/2021/09/Screenshot-2021-09-28-at-00.53.45.jpg?resize=768%2C423&ssl=1)

- As you can see, we now have access the registration token as shown below

> ![Getting Started](https://i0.wp.com/techdirectarchive.com/wp-content/uploads/2021/09/Screenshot-2021-09-28-at-00.56.52-1.jpg?resize=768%2C369&ssl=1)

Enter the Registration Token and
- Also enter a description for the runner. You can change this value later in the GitLab user interface.
- Enter the tags associated with the runner, separated by commas. You can change this value later in the GitLab user interface.

> ![Getting Started](https://i0.wp.com/techdirectarchive.com/wp-content/uploads/2021/09/Screenshot-2021-09-28-at-01.06.00.jpg?resize=768%2C339&ssl=1)

- Please enter the Runner executor such as: ssh, docker+machine, docker-ssh+machine, kubernetes, docker, parallels, virtualbox, docker-ssh, shell: docker. I will be using Shell as shown below.

> ![Getting Started](https://i0.wp.com/techdirectarchive.com/wp-content/uploads/2021/09/Screenshot-2021-09-28-at-01.09.11.jpg?resize=768%2C330&ssl=1) 

- Now you have successfully installed and registered GitLab runner as shown in the figure above.

- Note: If you entered docker as your executor, you’ll be asked for the default image to be used for projects that do not define one in .gitlab-ci.yml. You can enter alpine:latest

``` bash
Please enter the default Docker image (e.g. ruby:2.1):
alpine:latest
```

- You can see the GitLab Runner configuration in the config.toml file under the GitLab-Runner folder as shown below.

> ![Getting Started](https://i0.wp.com/techdirectarchive.com/wp-content/uploads/2021/09/Screenshot-2021-09-28-at-19.33.31.jpg?resize=768%2C457&ssl=1)

## <span>Check if the Runner is activated for the Project</span>

- Basically, you will have to follow the ssame steps as discussed previously. Click on the GitLab project and navigate to Settings > CI/CD and expand the Runners section.

> ![Getting Started](https://i0.wp.com/techdirectarchive.com/wp-content/uploads/2021/09/Screenshot-2021-09-28-at-00.49.54.jpg?w=579&ssl=1)

- As you can see below, the runner is activated for this project.

> ![Getting Started](https://i0.wp.com/techdirectarchive.com/wp-content/uploads/2021/09/Screenshot-2021-09-28-at-01.22.04.jpg?resize=768%2C418&ssl=1)