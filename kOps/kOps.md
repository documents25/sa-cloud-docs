
# KOPS  

## What is kOps ?

![Getting Started](https://cdn.appvia.io/media/pages/blog/contemplating-k8s-kops/145a434991-1647619677/1_xaswjs7wmp08xke_t7viow.png)

kOps, also known as Kubernetes operations, is an open-source project which helps you create, destroy, upgrade, and maintain a highly available, production-grade Kubernetes cluster. Kubernetes kOps is a free and open-source command-line tool for configuring and maintaining Kubernetes clusters and provisioning the cloud infrastructure needed to run them. kOps is mostly used in deploying AWS and GCE Kubernetes clusters. But officially, the tool only supports AWS. Support for other cloud providers (such as DigitalOcean, GCP, and OpenStack) are in the beta stage at the time of writing this article.

## Key Features 

- Full automation
- Cluster identification using DNS
- Broad operating system (OS) support
- Flexible provisioning
- Leverages

## How to Install kOps

```
curl -Lo kops https://github.com/kubernetes/kops/releases/download/v1.22.2/kops-linux-amd64
```
```
chmod +x kops
```
```
sudo mv kops /usr/local/bin/kops
```
## How to set up a Kubernetes Cluster in AWS with kOps

### Prerequisites

- An AWS account and the AWS CLI configured with admin credentials.
- kubectl.
- jq .
- An active domain with a dedicated “kops” subdomain

### Steps

<span style="color:gold ;text-transform: uppercase;"> Step-1 : Create the “kops” AWS IAM user
</span> 
> - Create an IAM user called “kops” with required permissions by running the following commands in the AWS CLI: 
```
aws iam create-group --group-name kops;

aws iam attach-group-policy --policy-arn arn:aws:iam::aws:policy/AmazonEC2FullAccess --group-name kops;
aws iam attach-group-policy --policy-arn arn:aws:iam::aws:policy/AmazonRoute53FullAccess --group-name kops; 
aws iam attach-group-policy --policy-arn arn:aws:iam::aws:policy/AmazonS3FullAccess --group-name kops; 
aws iam attach-group-policy --policy-arn arn:aws:iam::aws:policy/IAMFullAccess --group-name kops;
aws iam attach-group-policy --policy-arn arn:aws:iam::aws:policy/AmazonVPCFullAccess --group-name kops;
aws iam attach-group-policy --policy-arn arn:aws:iam::aws:policy/AmazonSQSFullAccess --group-name kops;
aws iam attach-group-policy --policy-arn arn:aws:iam::aws:policy/AmazonEventBridgeFullAccess --group-name kops;

aws iam create-user --user-name kops;
aws iam add-user-to-group --user-name kops --group-name kops; 
aws iam create-access-key --user-name kops;
```
> - Record the SecretAccessKey and AccessKeyID values in the output of the previous command, and then use them in the commands below:
```
# configure the AWS CLI to use ‘kops’ user

aws configure       # use the new access and secret key  
aws iam list-users  # you should see a list of all your IAM users here  

#Export the following variables for a session: 

export AWS_ACCESS_KEY_ID=$(aws configure get aws_access_key_id)  
export AWS_SECRET_ACCESS_KEY=$(aws configure get aws_secret_access_key)
```

<span style="color:gold ;text-transform: uppercase;"> Step-2 : Configure DNS setup
</span> 
> - Next, create a hosted zone in AWS for your kops subdomain:

```
#install jq locally before running the below command
aws route53 create-hosted-zone --name kops.yourdomain.com --caller-reference $(uuidgen) | jq .DelegationSet.NameServers
```
> - you should see output similar to this:

![Getting Started](./images/ns.png)

> - Next, you need to add these NS records for the subdomain in the DNS setting of the main domain.

![Getting Started](./images/ns-record.png)

> - Another way of verifying the NS records is by using the dig tool shown below:
```
dig ns kops.yourdomain.com +short
```
![Getting Started](./images/dig-ns.png)

<span style="color:gold ;text-transform: uppercase;"> Step-3 : Create cluster state storage
</span>

> - kOps stores its configurations, keys, and related items, in an S3 bucket to manage Kubernetes clusters. You need to create a dedicated S3 bucket for this purpose. Run the below command to create an S3 bucket named “kops-state-storage-cluster”:

    aws s3api create-bucket \
    --bucket kops-state-storage-cluster \
    --region ap-south-1 \
    --create-bucket-configuration LocationConstraint=ap-south-1

<span style="color:gold ;text-transform: uppercase;"> Step-4 : Install kOps
</span>
    
    curl -Lo kops https://github.com/kubernetes/kops/releases/download/v1.22.2/kops-linux-amd64
    chmod +x kops
    sudo mv kops /usr/local/bin/kops

<span style="color:gold ;text-transform: uppercase;"> Step-5 : Create the Kubernetes cluster
</span>
> - The next step is to create the Kubernetes cluster. First, you need to set up the name and point to the S3 bucket so that kOps can be aware of it.

    export NAME=kops.<domain name>
    export KOPS_STATE_STORE=s3://<bucket name>

> - Then, create the Kubernetes cluster in your favorable region and zone, for example :
<br>“ap-south-1” region and under the “ap-south-1a” zone. 

    kops create cluster \
    --zones=ap-south-1a \
    --name ${NAME}

> - Verify you see your cluster listed with this command:
    
    kops get cluster

> - Now, create the Kubernetes cluster with this command:

    kops update cluster --name kops.yourdomain.com --yes --admin

> - As a precaution, it is safer run in 'preview' mode first using kops update cluster --name , and once confirmed the output matches your expectations, you can apply the changes by adding --yes to the command - kops update cluster --name  --yes

> - Now you can interact with the Kubernetes cluster using kubectl. For example:

    kubectl get nodes

> - You can see that two instances are running—one master and one node.

![Getting Started](./images/nodes.png)

> - Let us deploy a simple Nginx workload and see if you can load the website in a browser.

    kubectl create deployment my-nginx --image=nginx --replicas=1 --port=80; 
    kubectl expose deployment my-nginx --port=80 --type=LoadBalancer;

> - Verify if the Nginx pods are running:

    kubectl get pods
![Getting Started](./images/get.png)

> - Get the Load Balancer (LB) address:

    kubectl get svc my-nginx
![Getting Started](./images/svc.png)

> - Here, <span style="color:gold ;">a6c88b3464bb64878a95dd2138c2eef7-341754367.ap-south-1.elb.amazonaws.com
</span> is the DNS name (endpoint) of the LB. Copy and paste it into a browser. You should see an Nginx default page:

![Getting Started](./images/result.png)

<span style="color:gold ;text-transform: uppercase;"> Step-6 :  Modify the Kubernetes cluster
</span>
> - Now, let’s increase the node count of the cluster from 1 node to 3 nodes. 

![Getting Started](./images/tk.png)

> - Here, the instance group name ‘nodes-ap-south-1a’ is for the node role. You can edit it and update the ‘maxSize’ and ‘minSize’ to values 2. First, open the editor with this command:

    kops edit instancegroups <id>
> - Then, find and edit the ‘maxSize’ and ‘minSize’ Save and quit the editor. Apply the changes by running:

    kops update cluster --name kops.<domain name> --yes --admin
> - After a few minutes, you can verify that the node count is 3.

<span style="color:gold ;text-transform: uppercase;"> Step-7 :  Delete the demo cluster and resources
</span>
> - Since you are running a Kubernetes cluster in AWS, the underlying infrastructure such as EC2 instances and LoadBalancers come with a cost. Run these following commands to delete the resources as well as the cluster:

    kubectl delete svc my-nginx;
    kubectl delete deploy my-nginx;
    kops delete cluster --name ${NAME} --yes;


## Conclusion 
kOps is a versatile tool for Kubernetes cluster management. It provides an automated way to provision your cluster’s underlying resources such as instances, load balancers, security groups, and volumes. Kubernetes clusters and cloud infrastructure (including those you manage with kOps) come with a cost.
