# Helm 
## Introduction

> Helm is a Kubernetes deployment tool for automating creation, packaging, configuration, and deployment of applications and services to Kubernetes clusters.

### Prerequisites
> The following prerequisites are required for a successful and properly secured use of Helm.

> A Kubernetes cluster Deciding what security configurations to apply to your installation, if any

> Installing and configuring Helm.

 
## Installation Of Helm

### From Script: 

> - curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
> - chmod 700 get_helm.sh
> - ./get_helm.sh



### From Apt (Debian/Ubuntu): 

> - curl https://baltocdn.com/helm/signing.asc | gpg --dearmor | sudo tee /usr/share/keyrings/helm.gpg > /dev/null
> - sudo apt-get install apt-transport-https --yes
> - echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/helm.gpg] https://baltocdn.com/helm/stable/debian/ all main" | sudo tee /etc/apt/sources.list.d/helm-stable-debian.list
> - sudo apt-get update
> - sudo apt-get install helm


### From Chocolatey (Windows):

> - choco install kubernetes-helm


## Structure Of Helm:

![images-1](https://razorops.com/images/blog/helm-3-tree.png)




## Advantage Of Helm:

> - Deployment Speed
> - Helm chart on Kubernetes for application configuration
> - Application testing


 
## DEMO

Values.yaml
```


replicaCount: 1

image:
  repository: nginx
  pullPolicy: IfNotPresent
  tag: ""

imagePullSecrets: []
nameOverride: ""
fullnameOverride: ""

serviceAccount:
  create: true
  annotations: {}
  name: ""

podAnnotations: {}

service:
  type: ClusterIP
  port: 80

ingress:
  enabled: true
  className: ""
  annotations: {}
    
  hosts:
    - host: dharmiknakrani.tech
      paths:
        - path: /
          pathType: ImplementationSpecific
  tls: []
 
autoscaling:
  enabled: false
  minReplicas: 1
  maxReplicas: 100
  targetCPUUtilizationPercentage: 80
  targetMemoryUtilizationPercentage: 80

nodeSelector: {}

```


deployment.yaml
```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "mychart.fullname" . }}
  labels:
    {{- include "mychart.labels" . | nindent 4 }}
spec:
  {{- if not .Values.autoscaling.enabled }}
  replicas: {{ .Values.replicaCount }}
  {{- end }}
  selector:
    matchLabels:
      {{- include "mychart.selectorLabels" . | nindent 6 }}
  template:
    metadata:
      {{- with .Values.podAnnotations }}
      annotations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      labels:
        {{- include "mychart.selectorLabels" . | nindent 8 }}
    spec:
      {{- with .Values.imagePullSecrets }}
      imagePullSecrets:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      serviceAccountName: {{ include "mychart.serviceAccountName" . }}
      securityContext:
        {{- toYaml .Values.podSecurityContext | nindent 8 }}
      containers:
        - name: {{ .Chart.Name }}
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: "{{ .Values.image.repository }}:{{ .Values.image.tag | default .Chart.AppVersion }}"
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          ports:
            - name: http
              containerPort: 80
              protocol: TCP
          livenessProbe:
            httpGet:
              path: /
              port: http
          readinessProbe:
            httpGet:
              path: /
              port: http
          resources:
            {{- toYaml .Values.resources | nindent 12 }}
      {{- with .Values.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.affinity }}
      affinity:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
```

## Extra Helm Important Command

> - helm list  # Show All Helm Chart Created in System
> - helm history RELEASE_NAME # Show History Of Specific helm Chart
> - helm upgrade [RELEASE] [CHART] [flags] # Upgrade Existing Helm Chart
> - helm uninstall RELEASE_NAME  # remove helm Chart
> - helm dependency update CHART # update dependency of Chart
> - helm get values RELEASE_NAME # get value of Chart
> - helm status RELEASE_NAME # Show Last status of Chart
