# Helm Best Practices

## Chart Names
 ```
  Chart names should only contain lowercase letters (and, if necessary, numbers). Also, if your name has to have more than one word, separate these words with hyphens -, for example, 
  
  Valid :
      nginx
      wordpress
      wordpress-on-nginx
  Invalid :
      Nginx
      Wordpress
      wordpressOnNginx
      wordpress_on_nginx
```
## Version Numbers

  version numbers are in the form of 1.2.3 (MAJOR.MINOR.PATCH). The first number represents the major version number, the second the minor, the third the patch number.

## values.yaml file

### Variable Names
```
  All variable names in the values.yaml file should begin with lowercase letters. for example; 
  
  Valid : 
      enabled: false
  
  Invalid :
      Enabled: false
  Often, your variable will need to contain multiple words to better describe it. Use what is called "camelcase": the first word starts with a lowercase letter, but the next ones all start with a capital letter.
  Valid :
      replicaCount: 3
      wordpressUsername: user
      wordpressSkipInstall: false 
  Invalid :
      ReplicaCount: 3
      wordpress-username: user
      wordpress_skip_install: false
```

## Template Files
### Names
    Template file names should be all lowercase letters. If they contain multiple words, these should be separated with dashes.        
    Valid :
        deployment.yaml
        tls-secrets.yaml
    Invalid :
        Deployment.yaml
        tls_secrets.yaml

## Dependencies
### Versions
    When your chart depends on other charts, you will need to declare in Chart.yaml the versions of these dependency charts. Instead of using an exact version like:
    Valid :
        version: ~1.8.2
    Invalid :
        version: 1.8.2

### URLs
> -  Use https:// repository URLs, instead of http:// when they are available. https’s encryption and certificate authentication has some positive side effects. For example, they make it harder for someone to perform a man-in-the-middle attack and poison you with their modified, malicious dependency charts.

## Recommended Labels
> -  Kubernetes or Kubernetes operators can find stuff by looking at metadata of some objects, more specifically, the labels in the metadata section. For example, there are lots of objects in the Kubernetes cluster. But only part of them are installed by and managed by Helm. Adding a label like helm.sh/chart: nginx-0.1.0 makes it easy for operators to find all objects installed by the nginx chart, version 0.1.0.  

```
Your templates should, preferably, generate templates with labels similar to this:

apiVersion: apps/v1
kind: Deployment
metadata:
  name: RELEASE-NAME-nginx
  labels:
    helm.sh/chart: nginx-0.1.0
    app.kubernetes.io/name: nginx
    app.kubernetes.io/instance: RELEASE-NAME
    app.kubernetes.io/version: "1.16.0"
    app.kubernetes.io/managed-by: Helm
spec:
  replicas: 1
  selector:
    matchLabels:
      app.kubernetes.io/name: nginx
      app.kubernetes.io/instance: RELEASE-NAME
  template:
    metadata:
      labels:
        app.kubernetes.io/name: nginx
        app.kubernetes.io/instance: RELEASE-NAME
    spec:
      serviceAccountName: RELEASE-NAME-nginx
      securityContext:
        {}
      containers:
        - name: nginx
          securityContext:
            {}
          image: "nginx:1.16.0"
          imagePullPolicy: IfNotPresent
          ports:
            - name: http
              containerPort: 80
              protocol: TCP
          livenessProbe:
            httpGet:
              path: /
              port: http
          readinessProbe:
            httpGet:
              path: /
              port: http
          resources:
            {}
```
